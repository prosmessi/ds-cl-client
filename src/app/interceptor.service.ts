import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {finalize, tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable()
export class InterceptorService implements HttpInterceptor {

    authToken: string;

    constructor() {

    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (localStorage.getItem('loggedInUser')) {
            this.authToken = JSON.parse(localStorage.getItem('loggedInUser')).access_token;
        } else {
            this.authToken = '';
        }
        const started = Date.now();
        let ok: string;

        if (!req.headers.has('Content-Type')) {
            req = req.clone({headers: req.headers.set('Content-Type', 'application/json')});
        } else {
            req = req.clone({headers: req.headers.delete('Content-Type')});
        }

        if (!req.headers.has('Accept')) {
            req = req.clone({headers: req.headers.set('Accept', 'application/json')});
        }

        // todo add token from Local storage
        if (localStorage.getItem('loggedInUser')) {
            req = req.clone({headers: req.headers.set('x-auth-token', this.authToken)});
        }

        return next.handle(req).pipe(
            tap(
                // Succeeds when there is a response; ignore other events
                event => ok = event instanceof HttpResponse ? 'succeeded' : '',
                // Operation failed; error is an HttpErrorResponse
                (error) => {
                    ok = error.status + 'failed';
                    if (error.status === 401) {
                        // this.logoutUser();
                        // this._globalService.logoutUser();
                    }
                }
            ),
            // Log when response observable either completes or errors
            finalize(() => {
                const elapsed = Date.now() - started;
                const msg = `${req.method} "${req.urlWithParams}"
             ${ok} in ${elapsed} ms.`;
                // console.log(msg);
                // this.loaderService.unloadData('Please wait...');
                // if (intervalTimeout) {
                //     clearInterval(intervalTimeout);
                // }
            })
        );
    }
}
