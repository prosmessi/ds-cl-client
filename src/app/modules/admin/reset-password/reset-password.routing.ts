import { Route } from '@angular/router';
import { ResetPasswordComponent } from 'app/modules/admin/reset-password/reset-password.component';

export const authResetPasswordRoutes: Route[] = [
    {
        path     : '',
        component: ResetPasswordComponent
    }
];
