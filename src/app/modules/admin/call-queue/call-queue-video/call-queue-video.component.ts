/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/type-annotation-spacing */
/* eslint-disable @typescript-eslint/naming-convention */
import {AppointmentService} from '../../../../services/appointment/appointment.service';
import {Router} from '@angular/router';
import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {merge} from 'rxjs';
import {map, startWith, switchMap} from 'rxjs/operators';
import { AgoraClient, ClientEvent, NgxAgoraService, Stream, StreamEvent } from 'ngx-agora';
declare let $:any;
declare function initClientAndJoinChannel(agoraAppId, token, channelName, uid) : any ;
@Component({
    selector: 'app-call-queue-video',
    templateUrl: './call-queue-video.component.html',
    styleUrls: ['./call-queue-video.component.scss']
})
export class CallQueueVideoComponent implements OnInit {

    items = ['Patient Profile', 'Allergies', 'Medications', 'Vitals', 'Symptoms', 'E-Prescription', 'Chat'];
    expandedIndex = 0;
    localCallId = 'agora_local';
    remoteCalls: string[] = [];
    // private client: AgoraClient;
	// private localStream: Stream;
    agoraAppId = 'bf67f816845a48f9b4d65ddb47f2c3e3';
    token = '00684e3a51ecf894db0a3330ab346638883IABr118SQOaabF9h3KazdaxQFsPEuLGwCicHOE2sqf3V+WPPSSEAAAAAEACcjToMmIoYYQEAAQCTihhh';
    channelName = 'nymtest1';
        /*
    * JS Interface for Agora.io SDK
    */

    // video profile settings
    cameraVideoProfile = '480p_4'; // 640 × 480 @ 30fps & 750kbs
    screenVideoProfile = '480p_2'; // 640 × 480 @ 30fps

    isLoading: boolean = false;
    callLogsTableColumns: string[] = ['name', 'phone', 'callType', 'date', 'checkIn', 'join', 'action'];
    callLogs: any = [];
    callLogsCount: number = 0;
    uuid: string;
    logindata: any;
    first_name: string;
    phone: string;
    email : string;
    allergies : any = [];
    medications : any = [];
    vitals : any = [];
    symptoms : string;
    time : string;
    accessToken : string;
    showModal : boolean;
    audioimage = false;
    videoimage = false;
    connected = false;
    role: string;
    private uid: number;
	  private client: AgoraClient;
	  private localStream: Stream;
     //private ngxAgoraService: NgxAgoraService

    constructor(
         private ngxAgoraService: NgxAgoraService,
        private _appointmentService: AppointmentService,
        private _router: Router
    ) {
        this.role = localStorage.getItem('role');
        this.uid = Math.floor(Math.random() * 1000000);
    }

    ngOnInit(): void {
      const today = new Date();
      this.time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
      const patientdata = JSON.parse(localStorage.getItem('listItem'));
      const doctorData = JSON.parse(localStorage.getItem('doctorItem'));
      
      if(this.role=='doctor'){
        this.first_name  = patientdata.pf_name;
        this.phone = patientdata.p_phone;
        this.email = patientdata.p_email;
        this.allergies = (patientdata.allergies ? JSON.parse(patientdata.allergies) : '');
        this.medications = (patientdata.medications ? JSON.parse(patientdata.medications) : '');
        this.vitals = {
          height: patientdata?.height,
          weight: patientdata?.weight,
          temperature: patientdata?.heart_rate_variation,
          oxygen_blood_saturation: patientdata?.oxygen_blood_saturation,
          heart_rate: patientdata?.heart_rate
        };
        this.channelName = patientdata.chanel_name;
        this.symptoms = patientdata.symptoms;
        this.getRtcAccessToken(this.channelName,this.uid);
      this.updateCallStatus(patientdata.call_id,'In progress');
      }else{
        this.first_name  = doctorData.first_name;
        this.phone = doctorData.phone;
        this.email = doctorData.email;
        // this.allergies = (patientdata.call_detail.allergies ? JSON.parse(patientdata.call_detail.allergies) : '');
        // this.medications = (patientdata.call_detail.medications ? JSON.parse(patientdata.call_detail.medications) : '');
        // this.vitals = (patientdata.call_detail.vitals ? JSON.parse(patientdata.call_detail.vitals) : '');
        this.channelName = patientdata.chanel_name;
        // this.symptoms = patientdata.call_detail.symptoms;
        this.getRtcAccessToken(this.channelName,this.uid);
    //   this.updateCallStatus(patientdata.call_id,'In progress');
      }
    //   this.first_name  = patientdata.p_user.first_name;
    //   this.phone = patientdata.p_user.phone;
    //   this.email = patientdata.p_user.email;
    //   this.allergies = (patientdata.call_detail.allergies ? JSON.parse(patientdata.call_detail.allergies) : '');
    //   this.medications = (patientdata.call_detail.medications ? JSON.parse(patientdata.call_detail.medications) : '');
    //   this.vitals = (patientdata.call_detail.vitals ? JSON.parse(patientdata.call_detail.vitals) : '');
    //   this.channelName = patientdata.chanel_name;
    //   this.symptoms = patientdata.call_detail.symptoms;
      //initClientAndJoinChannel(this.agoraAppId, this.token, this.channelName, this.uid);
      
    }

// doctor​/updated​/callStatus
     /**
      * Get common​/getRtcAccessToken
      * return {Void}
      */

      updateCallStatus(call_id,status): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    call_id: call_id,
                    status : status
                };
                return this._appointmentService.updateCallStatus(params);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                console.log(data['result'].length);
                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                console.log(data);
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }



     /**
      * Get common​/getRtcAccessToken
      * return {Void}
      */

      getRtcAccessToken(channelName,uid): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    channelName: channelName,
                    uid : uid
                };
                return this._appointmentService.getRtcAccessToken(params);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                console.log(data['result'].length);
                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                console.log(data);
                this.accessToken = data;
                const patientdata = JSON.parse(localStorage.getItem('listItem'));

                this.client = this.ngxAgoraService.createClient({ mode: 'rtc', codec: 'h264' });
                this.assignClientHandlers();

                this.localStream = this.ngxAgoraService.createStream({ streamID: this.uid, audio: true, video: true, screen: false });
                // this.localStream.setVideoProfile('720p_3');
                this.localStream.setVideoEncoderConfiguration({
                // The video resolution.
                resolution: {
                    width: 640,
                    height: 480
                },
                // The video frame rate (fps). We recommend setting it as 15. Do not set it to a value greater than 30.
                frameRate: {
                    min: 15,
                    max: 30
                },
                // The video bitrate (Kbps). Refer to the video profile table below to set this parameter.
                bitrate: {
                    min: 400,
                    max: 2000
                }
                });
                this.assignLocalStreamHandlers();

                // Join and publish methods added in this step
                this.initLocalStream(() => this.join(uid => this.publish(), error => console.error(error)));

                //initClientAndJoinChannel(this.agoraAppId, data, this.channelName, this.uid);
                //this._router.navigate([`/call-queue/preview/${patientdata.id}/video`]);
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/

            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }


      /**
       * Attempts to connect to an online chat room where users can host and receive A/V streams.
       */
  join(onSuccess?: (uid: number | string) => void, onFailure?: (error: Error) => void): void {
    console.log(this.channelName);
    console.log(this.accessToken);
      this.client.join(this.accessToken,this.channelName, this.uid, onSuccess, onFailure);
    }

    /**
     * Attempts to upload the created local A/V stream to a joined chat room.
     */
    publish(): void {
      this.client.publish(this.localStream, err => console.log('Publish local stream error: ' + err));
    }

    private assignClientHandlers(): void {
      this.client.on(ClientEvent.LocalStreamPublished, (evt) => {
      console.log('Publish local stream successfully'+evt);
      });

      this.client.on(ClientEvent.Error, (error) => {
      console.log('Got error msg:', error.reason);
      if (error.reason === 'DYNAMIC_KEY_TIMEOUT') {
        this.client.renewChannelKey(
        '',
        () => console.log('Renewed the channel key successfully.'),
        renewError => console.error('Renew channel key failed: ', renewError)
        );
      }
      });

      this.client.on(ClientEvent.RemoteStreamAdded, (evt) => {
      const stream = evt.stream as Stream;
      this.client.subscribe(stream, { audio: true, video: true }, (err) => {
        console.log('Subscribe stream failed', err);
      });
      });

      this.client.on(ClientEvent.RemoteStreamSubscribed, (evt) => {
      const stream = evt.stream as Stream;
      const id = this.getRemoteId(stream);
      // if (!this.remoteCalls.length) {
        console.log(this.remoteCalls);

        this.remoteCalls.push(id);
        setTimeout(() => stream.play(id), 1000);
      // }
      });

      this.client.on(ClientEvent.RemoteStreamRemoved, (evt) => {

      const stream = evt.stream as Stream;
      if (stream) {
        stream.stop();
        this.remoteCalls = [];
        console.log(`Remote stream is removed ${stream.getId()}`);
      }
      });

      this.client.on(ClientEvent.PeerLeave, (evt) => {
      const stream = evt.stream as Stream;
      if (stream) {
        stream.stop();
        this.remoteCalls = this.remoteCalls.filter(call => call !== `${this.getRemoteId(stream)}`);
        console.log(`${evt.uid} left from this channel`);
      }
      });


    }

    private assignLocalStreamHandlers(): void {

      this.localStream.on(StreamEvent.MediaAccessAllowed, () => {
      console.log('accessAllowed');
      });

      // The user has denied access to the camera and mic.
      this.localStream.on(StreamEvent.MediaAccessDenied, () => {
      console.log('accessDenied');
      });
    }

    private initLocalStream(onSuccess?: () => any): void {

      this.localStream.init(
      () => {
        // The user has granted access to the camera and mic.
        this.localStream.play(this.localCallId);
        if (onSuccess) {
        this.connected = true;
        onSuccess();
        }
      },
      err => console.error('getUserMedia failed', err)
      );
    }

    private getRemoteId(stream: Stream): string {
      return `agora_remote-${stream.getId()}`;
    }

    muteAudio(){
        
      this.localStream.muteAudio();
      this.audioimage = true;
      }
    unmuteAudio(){
        
      this.localStream.unmuteAudio();
      this.audioimage = false;
    }
    muteVideo(){
        
      this.localStream.muteVideo();
      this.videoimage = true;
    }
    unmuteVideo(){
        
      this.localStream.unmuteVideo();
      this.videoimage = false;
    }
    endCall(){
      this.showModal = true;
    }
    hide()
    {
      this.showModal = false;
    }
    endcallyes(){
      const call_id = JSON.parse(localStorage.getItem('listItem')).call_id;
      const channel_name = JSON.parse(localStorage.getItem('listItem')).channel_name;
      const datastring1 = {
        uid: this.uid.toString(),
        chanelName: channel_name,
        resourceid : sessionStorage.getItem('resourceId'),
        sid : sessionStorage.getItem('sid')
      };
      // this.restApi.stopCallRecording(datastring1).subscribe((data:any) => {
      //   if(data.status_code != 200){
      //     // this.showToast('danger','Online Doctor',data.status_message);
      //   }else{
      //     // 
      //     this.restApi.updateRecordingname({callId:call_id,name:data.result.serverResponse.fileList}).subscribe((data:any) => {
      //       // 
      //       if(data.status_code != 200){
      //         // this.showToast('danger','Online Doctor',data.status_message);
      //       }else{

      //         // this.showToast('success','Online Doctor',data.status_message);

      //       }
      //     })
      //     // this.showToast('success','Online Doctor',data.status_message);

      //   }
      // })

      //updateCallStatus
      const datastring = {
        status: 'Completed',
        callId: call_id
      };
      this.updateCallStatus(call_id,'Completed');
      this.localStream?.close();
      this.showModal = false;
      if(this.role=='doctor'){
        localStorage.setItem('listItem', localStorage.getItem('listItem'));
        this._router.navigate(['/doctors/create/prescription']);
      }else{
        this._router.navigate(['/dashboard']);
      }
      // this.restApi.updateCallStatus(datastring).subscribe((data:any) => {
      //   if(data.status_code != 200){

      //   }else{
      //     // this.socketService.callDisConnectByVet({call_id:call_id});
      //     this.localStream.close();
      //     // this.showModal = true;
      //     sessionStorage.setItem('callend','true');
      //     this._router.navigate(['/dashboard']);
      //   }
      // })
    }


}
