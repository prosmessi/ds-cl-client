import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {merge} from 'rxjs';
import {map, startWith, switchMap} from 'rxjs/operators';
import {GlobalService} from '../../../../services/global/global.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ShowErrorService} from '../../../../services/show-error/show-error.service';
import {PatientsService} from '../../../../services/patients/patients.service';
import {DoctorService} from '../../../../services/doctor/doctor.service';
@Component({
    selector: 'app-medical-details',
    templateUrl: './medical-details.component.html',
    styleUrls: ['./medical-details.component.scss']
    // encapsulation: ViewEncapsulation.None
})
export class MedicalDetailsComponent implements OnInit {

    isLoading: boolean = false;
    pageType: string;

    // Forms
    medicalInfoForm: FormGroup;
    // policyInfoForm: FormGroup;
    role: string;
    // state: Array<any> = [];
    // city: Array<any> = [];
    patients : any ;
    flag:any;
    // callloglist : any = [];
    allergies:any=[];
    medications:any=[];
    patient_id​: string;
    page​:number = 1;
    limit:number = 10;
    calllogCount : number;
    doctorItem: any ;
    agentItem:any;
    /**
     * Constructor
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private _formBuilder: FormBuilder,
        private _router: Router,
        private _patientsService: PatientsService,
        private _doctorService: DoctorService,
        private _matSnackBar: MatSnackBar,
        private _showErrorService: ShowErrorService,
        private _globalService: GlobalService,
    ) {
        this.role = localStorage.getItem('role');
        this.flag = localStorage.getItem('is_appointment');

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // this.getState();
        // this.pageType = this._activatedRoute.snapshot.paramMap.get('handle');
        // this.createForm();
        this.patients = JSON.parse(localStorage.getItem('listItem'));
        this.doctorItem = JSON.parse(localStorage.getItem('doctorItem'));
        this.agentItem = JSON.parse(localStorage.getItem('loggedInUser'));
        
        this.medicalInfoForm = this._formBuilder.group({
            accelerometer: [''],
            heart_rate_variation: [ ''],
            // title: [this.patients && this.patients.title || ''],
            heart_rate: [''],
            oxygen_blood_saturation: [''],
            allergies: [''],
            medications: [''],
            symptoms: [''],
            // language: [this.patients && this.patients.language || '', [Validators.required]],
            // gender: [this.patients && this.patients.gender && this.patients.gender.toLowerCase() || '', [Validators.required]],
            // aadhaar_number: [this.patients && this.patients.aadhaar_number || '', [Validators.required]],
            // licence: [this.doctor && this.doctor.licence || '', [Validators.required]],
            // dateOfBirth: [this.patients && this.patients.dateOfBirth || ''],
            // city: [this.patients && this.patients.city || '', [Validators.required]],
            // state: [this.patients && this.patients.state || '', [Validators.required]],
            // zip_code: [this.patients && this.patients.zip_code || '', [Validators.required]],
            // bio: [this.patients && this.patients.bio || '', [Validators.required]],
            // education: [this.doctor && this.doctor.education || ''],
        });
        // this.personalInfoForm.controls['state'].valueChanges.subscribe((value) => {
        //     
        //    this.getCity(value);
        //     // this.models = ... // here you add models to variable models based on selected make
        //   });
        // this.patient_id​ = this.patients.id;
        // this.policyInfoForm = this._formBuilder.group({
        //     _id: [''],
        //     policyNumber: ['', [Validators.required]],
        //     policyExpiryDate: [''],
        //     memberId: [''],
        //     companyNumber: [''],
        //     memberActive: [''],
        //     islamicDate: [''],
        //     policyDeductible: [''],
        //     city: [''],
        // });
        
    }

    allergyKeyPress(e) {
        if(e.key === 'Enter') {
            e.stopPropagation();
            this.addAllergies(e);
            e.target.focus();
        }
    }

    medicationKeyPress(e) {
        if(e.key === 'Enter') {
            e.stopPropagation();
            this.addMedication(e);
            e.target.focus();
        }
    }

    // handleEnterKeyPress(e) {
    //     e.preventDefault()
    // }

    addAllergies(button): void {

        // this.isLoading = true;
        button.target.disabled = true;
        // const data = this.personalInfoForm.value;
        // data.hospital_id =   JSON.parse(localStorage.getItem('loggedInUser')).hospital_id!="0"?JSON.parse(localStorage.getItem('loggedInUser')).hospital_id:'';
        if (this.medicalInfoForm.value.allergies != '') {
            this.allergies.push({id: this.allergies.length, name: this.medicalInfoForm.value.allergies})
            this.medicalInfoForm.get('allergies').patchValue('');
            button.target.disabled = false;
        }
    }
    addMedication(button): void {
        // this.isLoading = true;
        button.target.disabled = true;
        // const data = this.personalInfoForm.value;
        // data.hospital_id =   JSON.parse(localStorage.getItem('loggedInUser')).hospital_id!="0"?JSON.parse(localStorage.getItem('loggedInUser')).hospital_id:'';
        if (this.medicalInfoForm.value.medications != '') {
            this.medications.push({id: this.medications.length, name: this.medicalInfoForm.value.medications});
            this.medicalInfoForm.get('medications').patchValue('');
            button.target.disabled = false;
        }
    }
    makecall(button): void {

        this.isLoading = true;
        button.target.disabled = true;
        const data = this.medicalInfoForm.value;
        // console.log(data);
        data.doctor_id = this.doctorItem.users_id;
        data.patient_id=  this.patients.users_id;
        data.agent_id = this.agentItem.users_id;
        data.chanel_name = this.patients.first_name+Math.floor(100000 + Math.random() * 900000);
        data.device_type = 'web';
        data.allergies = JSON.stringify(this.allergies);
        data.medications = JSON.stringify(this.medications);
        // data.licence = this.educationForm.value.licence;
        // data.institution_name = this.educationForm.value.institution_name;
        // data.national_licence = this.educationForm.value.national_licence;
        // data.hospital_id =   JSON.parse(localStorage.getItem('loggedInUser')).hospital_id!="0"?JSON.parse(localStorage.getItem('loggedInUser')).hospital_id:'';

        console.log('[medical-details.component.ts || Line no. 324 ....]', this.flag);
        if(this.flag && this.flag == 'true'){
            // data
            var timedata = JSON.parse(localStorage.getItem('slottime'));
            data.start_time= timedata.start_time;
              data.end_time=timedata.end_time;
            data.date= timedata.date;
            data.appointment_type="VIDEO_CONSULT";
            data.device_type= "web";

            this._patientsService.scheduleAppointment(data).subscribe(
                (data) => {
                    if(data.status_code == 201){
                        this._globalService.showMessage(data.status_message);
                        // this._matSnackBar.open(data.status_message, 'OK', this._globalService._matSnackBarConfig);
                        this._router.navigate([`/dashboard`]);
                    }else{
                        this._globalService.showError(data.status_message)
                        // this._matSnackBar.open(data.status_message, 'OK', this._globalService._matSnackBarConfig);

                    }
                    setTimeout(() => {
                        this.isLoading = false;
                        button.target.disabled = false;
                    }, 500);

                },
                (err) => {
                    this._showErrorService.showError(err);
                    setTimeout(() => {
                        this.isLoading = false;
                        button.target.disabled = false;
                    }, 500);
                }
            );

        }else{
            this._patientsService.makeCall(data).subscribe(
                (data) => {
                    console.log('[medical-details.component.ts || Line no. 212 ....]', data);
                    if(data.status_code == 200){
                        // this._matSnackBar.open('New patient added', 'OK', this._globalService._matSnackBarConfig);
                        const item = JSON.parse(localStorage.getItem('listItem'));
                        item.call_id = data.result.call_id;
                        item.chanel_name = data.result.chanel_name;
                        console.log('[medical-details.component.ts || Line no. 216 ....]', data);
                        localStorage.setItem('listItem', JSON.stringify(item));
                        this._router.navigate([`/call-queue/preview/${this.patients.users_id}/video`]);
                    }else{
                        this._globalService.showError(data.status_message)
                        // this._matSnackBar.open(data.status_message, 'OK', this._globalService._matSnackBarConfig);
                    }
                    setTimeout(() => {
                        this.isLoading = false;
                        button.target.disabled = false;
                    }, 500);

                },
                (err) => {
                    this._showErrorService.showError(err);
                    setTimeout(() => {
                        this.isLoading = false;
                        button.target.disabled = false;
                    }, 500);
                }
            );

        }


    }
}
