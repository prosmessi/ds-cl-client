import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { merge } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { DoctorService } from '../../../../services/doctor/doctor.service';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import {AppointmentService} from '../../../../services/appointment/appointment.service';

@Component({
  selector: 'app-appointments-form',
  templateUrl: './appointments-form.component.html',
  styleUrls: ['./appointments-form.component.scss']
})
export class AppointmentsFormComponent implements OnInit {
  isLoading: boolean = false;
  doctorItem: any;
  instant_call: boolean = false
  selectedDate: any = new Date();
  today: Date;
  ctoday: string;
  videoCosulationduration: any;
  videoCosulationworkingHours: any;
  doctorsessiondata = [];
  mettingsessiondata: any;
  pipe = new DatePipe('en-US');
  allocatetimeslot = [];
  allotimeslot = [];
  currentTime: string;
  selectedTime: any;

  onSelect(event) {
    // console.log(event);
    this.selectedDate = event;
    // 
    var dd = String(event.getDate()).padStart(2, '0');
    var mm = String(event.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = event.getFullYear();
    this.ctoday = mm + '/' + dd + '/' + yyyy;
    this.getdayname(this.ctoday);
  }
  timeslot: any = [];
  constructor(private _router: Router, private _doctorService: DoctorService) { }
  // minDate(): string {
  //   return moment().add(-10, 'day') + '';
  // }
  // maxDate(): string {
  //   return moment().add(10, 'day') + '';
  // }
  ngOnInit(): void {
    this.doctorItem = JSON.parse(localStorage.getItem('doctorItem'));
    this.today = new Date();
    var dd = String(this.today.getDate()).padStart(2, '0');
    var mm = String(this.today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = this.today.getFullYear();
    this.ctoday = mm + '/' + dd + '/' + yyyy;
    localStorage.setItem('indivioctordulaDdata', JSON.stringify(this.doctorItem));
    var video_consultation = JSON.parse(this.doctorItem.video_consultation);
    this.videoCosulationduration = video_consultation.videoCosulationduration;
    localStorage.setItem('duration', this.videoCosulationduration);
    this.videoCosulationworkingHours = video_consultation.videoCosulationworkingHours;
    
    this.getdayname(this.ctoday);
  }
  showcurrentTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    // add a zero in front of numbers<10
    m = this.checkTime(m);
    s = this.checkTime(s);
    this.currentTime = h + ":" + m;
    return this.currentTime;
    // document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
    // const t = setTimeout(function() {
    //   this.showcurrentTime()
    // }, 500);
  }
  checkTime(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }
  getTimeStops(d, start, end, interval) {
    
    this.showcurrentTime();
    var startTime = moment(start, 'HH:mm');
    var endTime = moment(end, 'HH:mm');
    endTime.subtract(interval, 'minutes')
    if (endTime.isBefore(startTime)) {
      endTime.add(1, 'day');
    }

    var timeStops = [];

    while (startTime <= endTime) {
      timeStops.push(moment(startTime).format('HH:mm'));
      startTime.add(interval, 'minutes');
    }
    if (this.ctoday == d) {
      timeStops = timeStops.filter(data => data > this.currentTime)
    }
    return timeStops;
  }
  getTwentyFourHourTime(amPmString) {
    // amPmString = amPmString.split(' ')[0];
    var d = new Date("1/1/2013 " + amPmString);
    console.log(d.getHours() + ':' + (d.getMinutes() == 0 ? '00' : d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes()))
    return d.getHours() + ':' + (d.getMinutes() == 0 ? '00' : d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes());
  }

  getdayname(d) {
    localStorage.setItem('slotdate', d);
    var a = new Date(d);
    var days = new Array(7);
    days[0] = "Sunday";
    days[1] = "Monday";
    days[2] = "Tuesday";
    days[3] = "Wednesday";
    days[4] = "Thursday";
    days[5] = "Friday";
    days[6] = "Saturday";
    var day = days[a.getDay()];
    this.doctorsessiondata = (this.videoCosulationworkingHours.filter(function (i) { return i.dayname == day; }));
    this.mettingsessiondata = (this.doctorsessiondata[0].daystatus ? this.doctorsessiondata[0].session : []);
    var timeSlot = localStorage.getItem('duration');
    // timeslot
    this.timeslot = [];
    for (let index = 0; index < this.mettingsessiondata.length; index++) {
      //  const element = this.mettingsessiondata[index];
      var temparray;
      this.selectedTime = null;
      temparray = this.getTimeStops(d, this.getTwentyFourHourTime(this.mettingsessiondata[index].fromTime), this.getTwentyFourHourTime(this.mettingsessiondata[index].toTime), timeSlot);
      console.log('LIne 132 .temparray.', temparray, d, this.getTwentyFourHourTime(this.mettingsessiondata[index].fromTime), this.getTwentyFourHourTime(this.mettingsessiondata[index].toTime), timeSlot);
      this.timeslot = this.timeslot.length == 0 ? temparray : [...this.timeslot, ...temparray];
    }
    console.log('LIne 132 . .', this);
    if (this.mettingsessiondata.length > 0) {
      var mySimpleFormat = this.pipe.transform(d, 'yyyy-MM-dd');
      var indivioctordulaDdata = JSON.parse(localStorage.getItem('indivioctordulaDdata'));
      this._doctorService.getAllocatedTimeSlot(indivioctordulaDdata.users_id,mySimpleFormat).subscribe((finaldata:any) => {
        console.log('[appointments-form.component.ts || Line no. 147 ....]', finaldata);
        if (finaldata.status_code == 200) {
          this.allocatetimeslot = finaldata.result;
          for (let index = 0; index < this.allocatetimeslot.length; index++) {
            //  const element = this.mettingsessiondata[index];
            this.allotimeslot.push(this.getTwentyFourHourTime(this.allocatetimeslot[index].start_time));
          }
          this.timeslot = this.timeslot.filter(n => !this.allotimeslot.includes(n));
        }

      });
    } else {
      this.timeslot = [];
    }


  }
  tConvert(time) {

    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
      time = time.slice(1); // Remove full string match value
      time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join(''); // return adjusted time or original string
  }
  settime(time) {
   var end_timeT = moment(time, 'HH:mm');
       end_timeT.add(this.videoCosulationduration, 'minutes');
    var tempTime = {
      start_time: this.tConvert(time),
      end_time:this.tConvert(moment(end_timeT).format('HH:mm')),
      date: moment(this.ctoday +' '+ time).format('yyyy-MM-DD HH:mm:ss')
    };

    localStorage.setItem('slottime', JSON.stringify(tempTime));
    this.selectedTime = time;
    // if(this.checkUserStatus == true){
    // if(this.persistanceService.get('petlogindata') != null){
    //   this._router.navigate(['/pet-appointment']);
    //   }
    // else{
    // 	this.loginModal = true;
    // }
  }
  gotoMedicalPage() {
    if (this.instant_call) {
      localStorage.setItem('is_appointment', "false");
    } else {
      localStorage.setItem('is_appointment', "true");
    }
    this._router.navigate([`/patients/medical/details`]);
  }
}
