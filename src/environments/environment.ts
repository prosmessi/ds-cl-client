// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can b baseUrl: 'https://deshclinic.com:3001/',e found in `angular.json`.

export const environment = {
    production: false,
    // baseUrl: 'https://sisgain.com:3005/',
    // baseUrl: 'https://3e4a-223-225-19-213.ngrok.io/',
    baseUrl: 'https://192.168.1.11:3005/',
    baseImageUrl: 'https://192.168.1.11:3005/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
