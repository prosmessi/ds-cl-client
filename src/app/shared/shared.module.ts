import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppListHeaderComponent } from './components/app-list-header/app-list-header.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatButtonModule} from '@angular/material/button';
import {RouterModule} from '@angular/router';
import {MatInputModule} from '@angular/material/input';
import { AppInputInfoComponent } from './components/app-input-info/app-input-info.component';
import { TableExportToExcelDirective } from './directives/table-export-to-excel.directive';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        MatProgressBarModule,
        MatButtonModule,
        RouterModule,
        MatInputModule
    ],
	exports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		AppListHeaderComponent,
		AppInputInfoComponent,
		TableExportToExcelDirective
	],
    declarations: [
      AppListHeaderComponent,
      AppInputInfoComponent,
      TableExportToExcelDirective
    ]
})
export class SharedModule
{
}
