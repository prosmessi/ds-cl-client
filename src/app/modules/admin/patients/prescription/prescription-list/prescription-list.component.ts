import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-prescription-list',
    templateUrl: './prescription-list.component.html',
    styleUrls: ['./prescription-list.component.scss']
})
export class PrescriptionListComponent implements OnInit {

    isLoading: boolean = false;
    patients = [
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        }, {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        }, {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true
        }, {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        },


    ];
    prescriptionsTableColumns: string[] = ['name', 'email', 'phone', 'createdAt', 'action'];

    constructor() {
    }

    ngOnInit(): void {
    }

}
