import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {ApexOptions} from 'ng-apexcharts';
import {Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {Observable, Subject, timer} from 'rxjs';
import {ProjectService} from './project.service';
import {ClinicService} from './../../../services/clinic/clinic.service';

import * as moment from 'moment';
import {Pagination} from '../../../shared/classes/pagination.class';
import {AppointmentService} from '../../../services/appointment/appointment.service';
import {GlobalService} from '../../../services/global/global.service';
import {ShowErrorService} from '../../../services/show-error/show-error.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends Pagination implements OnInit, AfterViewInit, OnDestroy {
    public httpReq: (page: number, limit: number, queryField: string) => Observable<any>;

    todayDate = moment().toString();
    userInfo: any;
    data: any;
    role: string;
    isDashLoading: boolean;
    dashboardCount:any;
    activeAppointmentIndices = [];

    graphConfig = {
        visits: {
            value: 882,
            ofTarget: -9
        },
        chartType: 'bar',
        datasets: [
            {
                label: 'Old',
                backgroundColor: '#F49BA9',
                data: [35, 25]
            },
            {
                label: 'New',
                backgroundColor: '#EE4862',
                data: [40, 3]
            },
        ],
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
        colors: [
            {
                borderColor: '#EE4862',
                backgroundColor: '#F49BA9'
            }
        ],
        options: {
            // barValueSpacing: 20,
            scales: {
                xAxes: [
                    {
                        gridLines: {
                            display: false,
                            drawBorder: false,
                            tickMarkLength: 18
                        },
                        ticks: {
                            fontColor: '#000000'
                        }
                    }
                ],
                yAxes: [
                    {
                        gridLines: {
                            display: false,
                            drawBorder: false,
                            tickMarkLength: 18
                        },
                        ticks: {
                            fontColor: '#000000'
                        }
                    },
                ]
            }
        }
        /* options: {
             spanGaps: false,
             legend: {
                 display: false
             },
             maintainAspectRatio: false,
             layout: {
                 padding: {
                     top: 32,
                     left: 32,
                     right: 32
                 }
             },
             elements: {
                 point: {
                     radius: 4,
                     borderWidth: 2,
                     hoverRadius: 4,
                     hoverBorderWidth: 2
                 },
                 line: {
                     tension: 0
                 }
             },
             scales: {
                 xAxes: [
                     {
                         gridLines: {
                             display: false,
                             drawBorder: false,
                             tickMarkLength: 18
                         },
                         ticks: {
                             fontColor: '#000000'
                         }
                     }
                 ],
                 yAxes: [
                     {
                         display: true,
                     }
                 ]
             },
             plugins: {
                 filler: {
                     propagate: false
                 },
                 xLabelsOnTop: {
                     active: true
                 }
             }
         }*/
    };


    commonOption: any = {
        responsive: true,
        maintainAspectRatio: false,
        cutoutPercentage: 60,
        backgroundColor: '#FAA503',
        borderColor: '#9ebecd',
    };

    colors = [{
        borderColor: '#FAA503',
        backgroundColor: '#FAA503',
        pointBackgroundColor: '#FAA503',
        pointHoverBackgroundColor: '#FAA503',
        pointBorderColor: '#ffffff',
        pointHoverBorderColor: '#ffffff'
    }];

    salesDataset = [{
        label: 'This week',
        data: [30, 40, 50, 15, 60, 20, 40],
        backgroundColor: '#FAA503',
        borderColor: '#FAA503'
    }];

    doctorGraphConfig = {
        visits: {
            value: 882,
            ofTarget: -9
        },
        chartType: 'bar',
        datasets: [
            {
                label: 'Old',
                backgroundColor: '#FAA503',
                data: [35]
            },
            {
                label: 'New',
                backgroundColor: '#FAA503',
                data: [40]
            },
        ],
        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        colors: [
            {
                borderColor: '#FAA503',
                backgroundColor: '#F49BA9'
            }
        ],
        options: {
            // barValueSpacing: 20,
            scales: {
                xAxes: [
                    {
                        gridLines: {
                            display: false,
                            drawBorder: false,
                            tickMarkLength: 18
                        },
                        ticks: {
                            fontColor: '#000000'
                        }
                    }
                ],
                yAxes: [
                    {
                        gridLines: {
                            display: false,
                            drawBorder: false,
                            tickMarkLength: 18
                        },
                        ticks: {
                            fontColor: '#000000'
                        }
                    },
                ]
            }
        }
        /* options: {
             spanGaps: false,
             legend: {
                 display: false
             },
             maintainAspectRatio: false,
             layout: {
                 padding: {
                     top: 32,
                     left: 32,
                     right: 32
                 }
             },
             elements: {
                 point: {
                     radius: 4,
                     borderWidth: 2,
                     hoverRadius: 4,
                     hoverBorderWidth: 2
                 },
                 line: {
                     tension: 0
                 }
             },
             scales: {
                 xAxes: [
                     {
                         gridLines: {
                             display: false,
                             drawBorder: false,
                             tickMarkLength: 18
                         },
                         ticks: {
                             fontColor: '#000000'
                         }
                     }
                 ],
                 yAxes: [
                     {
                         display: true,
                     }
                 ]
             },
             plugins: {
                 filler: {
                     propagate: false
                 },
                 xLabelsOnTop: {
                     active: true
                 }
             }
         }*/
    };

    chartGithubIssues: ApexOptions = {
        chart: {
            fontFamily: 'inherit',
            foreColor: 'inherit',
            height: '100%',
            type: 'line',
            toolbar: {
                show: false
            },
            zoom: {
                enabled: false
            }
        },
        colors: ['#64748B', '#94A3B8'],
        dataLabels: {
            enabled: true,
            enabledOnSeries: [0],
            background: {
                borderWidth: 0
            }
        },
        grid: {
            borderColor: 'var(--fuse-border)'
        },
        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        legend: {
            show: false
        },
        plotOptions: {
            bar: {
                columnWidth: '50%'
            }
        },
        states: {
            hover: {
                filter: {
                    type: 'darken',
                    value: 0.75
                }
            }
        },
        stroke: {
            width: [3, 0]
        },
        tooltip: {
            followCursor: true,
            theme: 'dark'
        },
        xaxis: {
            axisBorder: {
                show: false
            },
            axisTicks: {
                color: 'var(--fuse-border)'
            },
            labels: {
                style: {
                    colors: 'var(--fuse-text-secondary)'
                }
            },
            tooltip: {
                enabled: false
            }
        },
        yaxis: {
            labels: {
                offsetX: -16,
                style: {
                    colors: 'var(--fuse-text-secondary)'
                }
            }
        }
    };

    /* Doctor Dashboard */

    appointmentsCount: number = 0;

    // appointmentsTableColumns: string[] = ['name', 'date', 'time', 'status', 'action'];
    appointmentsTableColumns: string[] = ['sno', 'memberName', 'doctorName', 'scheduleInfo', 'action'];
    appointments = [
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true,
            time: '11:30 AM'
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true,
            time: '11:30 AM'
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true,
            time: '11:30 AM'
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true,
            time: '11:30 AM'
        },

    ];

    private veryMinTimer = timer(0, 60*1000);

    private _unsubscribeAll: Subject<any> = new Subject<any>();

    constructor(
        private _globalService: GlobalService,
        private _projectService: ProjectService,
        private _appointmentService: AppointmentService,
        private _showErrorService: ShowErrorService,
        private _router: Router,
        private _clinicService: ClinicService
    ) {
        super();
        this.isDashLoading = true;
        this.limit = 5;
        this.httpReq = (page, limit, queryField): Observable<any> => this._globalService.fetchUpcomingAppointments(page, limit, queryField);
    }

    /**
     * On init
     */
    ngOnInit(): void {

        this.userInfo = JSON.parse(localStorage.getItem('loggedInUser'));
        // Get the loggedIn user Role
        this.role = localStorage.getItem('role');

        if(this.role === 'representative') {
            this.queryField = 'agent_id=' + this.userInfo.users_id;
        }
        if(this.role === 'doctor') {
            this.queryField = 'doctor_id=' + this.userInfo.users_id;
            this.appointmentsTableColumns.pop();
        }

        console.log('Role condition is not doctor ', this.role && this.role !== 'doctor');

        this.appointmentsCount = 1;
        // Get the data
        this._projectService.data$
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((data) => {
                // Store the data
                this.data = data;
                // Prepare the chart data
                this._prepareChartData();
            });

            this._clinicService.getDashboardCount(this.userInfo.user_type_id).subscribe((data) => {
                
                // console.log('[dashboard.component.ts || Line no. 447 ....]', data);
                this.dashboardCount = data.result;
                this.isDashLoading = false;
                // console.log('[dashboard.component.ts || Line no. 448 ....]', this.dashboardCount);
            });
        // Attach SVG fill fixer to all ApexCharts
        window['Apex'] = {
            chart: {
                events: {
                    mounted: (chart: any, options?: any): void => {
                        this._fixSvgFill(chart.el);
                    },
                    updated: (chart: any, options?: any): void => {
                        this._fixSvgFill(chart.el);
                    }
                }
            }
        };
    }

    ngAfterViewInit(): void {
        this.fetchForFirstTime().then(res => {

            this.veryMinTimer
              .pipe(takeUntil(this._unsubscribeAll))
              .subscribe(() => {
                  console.log('[dashboard.component.ts || Line no. 439 ....]', 'Timer - ran', this.currentItems)
                  // Do something with the appointment timer
                  this.currentItems.forEach((appointment, index) => {
                      if(this.activeAppointmentIndices.includes(appointment.id)) {
                          return;
                      }
                      console.log('[dashboard.component.ts || Line no. 483 ....]', appointment.date);
                    if(moment(appointment.date) <= moment()) {
                        this.activeAppointmentIndices.push(appointment.id);
                    }
                  });
              });
        });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Fix the SVG fill references. This fix must be applied to all ApexCharts
     * charts in order to fix 'black color on gradient fills on certain browsers'
     * issue caused by the '<base>' tag.
     *
     *
     * @param element
     * @private
     */
    private _fixSvgFill(element: Element): void {
        // Current URL
        const currentURL = this._router.url;

        // 1. Find all elements with 'fill' attribute within the element
        // 2. Filter out the ones that doesn't have cross reference so we only left with the ones that use the 'url(#id)' syntax
        // 3. Insert the 'currentURL' at the front of the 'fill' attribute value
        Array.from(element.querySelectorAll('*[fill]'))
            .filter(el => el.getAttribute('fill').indexOf('url(') !== -1)
            .forEach((el) => {
                const attrVal = el.getAttribute('fill');
                el.setAttribute('fill', `url(${currentURL}${attrVal.slice(attrVal.indexOf('#'))}`);
            });
    }

    /**
     * Prepare the chart data from the data
     *
     * @private
     */
    private _prepareChartData(): void {
        // Github issues
        // @ts-ignore

    }


    /**
     * joincall
     */
    appointmentMakeCall(button, appointment): void {

        this.isLoading = true;
        button.target.disabled = true;
        // debugger
        const data = {
            patient_id: appointment.patient_id,
            doctor_id: appointment.doctor_id,
            agent_id: appointment.agent_id,
            call_id: appointment.call_id,
            channel_name: appointment.p_first_name + Math.floor(100000 + Math.random() * 900000)
        };
        const item = JSON.parse(localStorage.getItem('listItem') || '{}');
        item.call_id = appointment.call_id;
        item.chanel_name = data.channel_name;
        // item.channel_name=
        localStorage.setItem('listItem', JSON.stringify(item));
        this._appointmentService.appointmentMakeCall(data).subscribe({
          next: (data) => {
              if(data.status_code == 200){
                  // this._matSnackBar.open('New patient added', 'OK', this._globalService._matSnackBarConfig);
                  this._router.navigate([`/call-queue/preview/${data.patient_id}/video`]);
              }else{
                  this._globalService.showError(data.status_message)
                  // this._matSnackBar.open(data.status_message, 'OK', this._globalService._matSnackBarConfig);
              }
              setTimeout(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              }, 500);

          },
          error: (err) => {
              this._showErrorService.showError(err);
              setTimeout(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              }, 500);
          }
        });
        // localStorage.setItem('listItem', JSON.stringify(doctor));
        // this._appointmentService._data = doctor;
        // this._router.navigate([`/call-queue/preview/${doctor.id}/video`]);
    }

}
