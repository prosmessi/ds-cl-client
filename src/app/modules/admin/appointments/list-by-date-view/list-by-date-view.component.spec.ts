import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListByDateViewComponent } from './list-by-date-view.component';

describe('ListByDateViewComponent', () => {
  let component: ListByDateViewComponent;
  let fixture: ComponentFixture<ListByDateViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListByDateViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListByDateViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
