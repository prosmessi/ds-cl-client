import { Component, OnInit, Input } from '@angular/core';
import {fadeAnimation} from '../../../../@fuse/animations/fade.animation';

@Component({
  selector: 'app-input-info',
  templateUrl: './app-input-info.component.html',
  styleUrls: ['./app-input-info.component.scss'],
  animations: [fadeAnimation]
})
export class AppInputInfoComponent implements OnInit {

  @Input() message: string | null;

  constructor() { }

  ngOnInit(): void {
  }

}
