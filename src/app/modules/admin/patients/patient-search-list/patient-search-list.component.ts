import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {merge, Observable} from 'rxjs';
import {map, startWith, switchMap} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PatientsService} from '../../../../services/patients/patients.service';
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {Pagination} from '../../../../shared/classes/pagination.class';
import {GlobalService} from '../../../../services/global/global.service';
import {TableExportToExcelDirective} from '../../../../shared/directives/table-export-to-excel.directive';

@Component({
    selector: 'app-patient-search-list',
    templateUrl: './patient-search-list.component.html',
    styleUrls: ['./patient-search-list.component.scss']
})
export class PatientSearchListComponent extends Pagination implements OnInit, AfterViewInit {
    public httpReq: (page: number, limit: number, queryField: string) => Observable<any>;

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    isLoading: boolean = false;
    searchForm: FormGroup;
    patientsCount: number = 1;
    patientsTableColumns: string[] = ['name', 'email', 'phone', 'status', 'createdAt', 'action'];
    patients: Array<any> = [];


    name: string = '';
    // doctorsCount: number = 0;
    role: string;

    constructor(
        private _patientService: PatientsService,
        private _formBuilder: FormBuilder,
        private _router: Router,
        private _globalService: GlobalService
    ) {
        super();
        this.role = localStorage.getItem('role');
        this.httpReq = (page, limit, queryField): Observable<any> => this._globalService.getUserSearch(page, limit, queryField + 'role=patient');
    }

    ngOnInit(): void {
        this.searchForm = this._formBuilder.group({
            search: '',
        })
        this.patientsCount = 1;
    }

    /**
     * After View Init
     */
    ngAfterViewInit(): void {
        // this.getPatientList('');
        this.fetchForFirstTime().then();
    }
    /**
     * Preview doctor's info
     *
     * @param patient
     */
    previewPatient(patient): void {
        localStorage.setItem('listItem', JSON.stringify(patient));
        this._patientService._patientData = patient;
        this._router.navigate([`/patients/preview/${patient.users_id}/view`]);
    }

    /**
     * Get Appointments List
     * return {Void}
     */
    getPatientList(data): void {
        
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._patientService.getPatientsSearch(params.page, params.limit,data);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                this.patientsCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }else{
                    return [];
                } 

            })
        ).subscribe(
            (data) => {
                // set response data
                this.patients = data;
                
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                this.patients =[];
                // show the error
                // console.log('err: ', err);
            }
        );
    }


    gotoDoctorSearch(data): void {
        localStorage.setItem('listItem', JSON.stringify(data));
        // this._doctorService._doctorData = data;
        this._router.navigate([`/doctors/search/list`]);
    }
}
