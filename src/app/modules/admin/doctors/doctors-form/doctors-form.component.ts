import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray,FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DoctorService} from '../../../../services/doctor/doctor.service';
import {GlobalService} from '../../../../services/global/global.service';
import {ShowErrorService} from '../../../../services/show-error/show-error.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {map, startWith, switchMap} from 'rxjs/operators';
import {browserRefresh} from "../../../../app.component";
import {merge} from 'rxjs';
import {ValidateForm} from '../../../../shared/classes/validation.class';
import {SpecialityService} from '../../../../services/speciality/speciality.service';

@Component({
    selector: 'app-doctors-form',
    templateUrl: './doctors-form.component.html',
    styleUrls: ['./doctors-form.component.scss']
    // encapsulation: ViewEncapsulation.None
})
export class DoctorsFormComponent implements OnInit {

    isLoading: boolean = false;
    pageType: string;
    doctorId: string;
    doctorForm: FormGroup;
    educationForm: FormGroup;
    doctor: any;
    specialites: Array<any> = [];
    specialityCount: number = 0;
    state: Array<any> = [];
    city: Array<any> = [];
    selectedTabIndex: number = 0;
    personalValidation: ValidateForm;
    educationValidation: ValidateForm;
    today = new Date();
  timer: any;

  @ViewChild('input') inputSearchSpecialities: ElementRef<HTMLInputElement>;

    /**
     * Constructor
     */
    constructor(
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _formBuilder: FormBuilder,
        private _doctorService: DoctorService,
        private _specialityService: SpecialityService,
        private _globalService: GlobalService,
        private _showErrorService: ShowErrorService,
        private _matSnackBar: MatSnackBar
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    // testdfh:any;
    ngOnInit(): void {
        //
      this.setSpecialities();
      // this._specialityService.searchSpeciality(0, 20).subscribe({
      //   next: (data) => {
      //     this.isLoading = false;
      //     console.log('[doctors-form.component.ts || Line no. 68 ....]', data);
      //     if (data.status_code === 200) {
      //       this.specialites = data['result'].data;
      //       // this.totalItemsCount = data['result'].count;
      //       // this.loadedData.push(...data['result'].data);
      //     }
      //     if (data.status_code === 404) {
      //       this.specialites = [];
      //     }
      //   }
      // })
        // this.getSpecialityList();
        this.getState();
        // 
        this.pageType = this._activatedRoute.snapshot.paramMap.get('handle') || '';
        this.doctorId = this._activatedRoute.snapshot.paramMap.get('doctorId') || '';

        this.doctor = this._doctorService._doctorData;
        if (browserRefresh && JSON.parse(localStorage.getItem('listItem'))) {
            this.doctor = JSON.parse(localStorage.getItem('listItem'));
        }
        if(this.pageType != 'edit'){
            this.doctor = null;
        }
        this.doctorForm = this._formBuilder.group({
          uuid: [this.doctor && this.doctor.uuid || ''],
          email: [this.doctor && this.doctor.email || '', [Validators.required, Validators.email]],
          title: [this.doctor && this.doctor.title || 'Dr', [Validators.required]],
          first_name: [this.doctor && this.doctor.first_name || '', [Validators.required, Validators.minLength(2)]],
          middle_name: [this.doctor && this.doctor.middle_name || '',],
          last_name: [this.doctor && this.doctor.last_name || '', [Validators.required, Validators.minLength(2)]],
          phone: [this.doctor && this.doctor.phone || '', [Validators.required, Validators.pattern('[0-9]+'), Validators.minLength(10), Validators.maxLength(11)]],
          address: [this.doctor && this.doctor.address1 || '', [Validators.required, Validators.minLength(3)]],
          language: [this.doctor && this.doctor.language || '', [Validators.required]],
          gender: [this.doctor && this.doctor.gender || '', [Validators.required]],
          speciality: [this.doctor && parseInt(this.doctor.speciality, 10) || '', [Validators.required]],
          // licence: [this.doctor && this.doctor.licence || '', [Validators.required]],
          date_of_birth: [this.doctor && this.doctor.date_of_birth || '', [Validators.required]],
          city: [{value: this.doctor && this.doctor.city || '', disabled: !this.doctor?.city}, [Validators.required]],
          state: [this.doctor && this.doctor.state || '', [Validators.required]],
          zip_code: [this.doctor && this.doctor.zip_code || '', [Validators.required, Validators.pattern('[0-9]+'), Validators.minLength(3), Validators.maxLength(15)]],
          bio: [this.doctor && this.doctor.bio || ''],
            // uuid: [this.doctor && this.doctor.uuid || ''],
            // email: [this.doctor && this.doctor.email || '', [Validators.required]],
            // title: [this.doctor && this.doctor.	title || 'Dr'],
            // first_name: [this.doctor && this.doctor.first_name || '', [Validators.required]],
            // middle_name: [this.doctor && this.doctor.middle_name || ''],
            // last_name: [this.doctor && this.doctor.last_name || ''],
            // phone: [this.doctor && this.doctor.phone || '', [Validators.required]],
            // address: [this.doctor && this.doctor.address1 || '', [Validators.required]],
            // language: [this.doctor && this.doctor.language || '', [Validators.required]],
            // gender: [this.doctor && this.doctor.gender && this.doctor.gender.toLowerCase() || '', [Validators.required]],
            // speciality: [this.doctor && parseInt(this.doctor.speciality) || '', [Validators.required]],
            // // licence: [this.doctor && this.doctor.licence || '', [Validators.required]],
            // // dateOfBirth: [this.doctor && this.doctor.dateOfBirth || ''],
            // date_of_birth: [this.doctor && this.doctor.date_of_birth || '', [Validators.required]],
            // city: [this.doctor && this.doctor.city || '', [Validators.required]],
            // state: [this.doctor && this.doctor.state || '', [Validators.required]],
            // zip_code: [this.doctor && this.doctor.zip_code || '', [Validators.required]],
            // bio: [this.doctor && this.doctor.bio || '', [Validators.required]]
            // data.education_details = this.educationForm.value.education_details;
        });

        if(this.doctorForm.value.state && this.doctorForm.value.state!=''){
            this.getCity(this.doctorForm.value.state);
        }
        this.doctorForm.controls['state'].valueChanges.subscribe((value) => {
          // // debugger
          // console.log(value);
          this.doctorForm.controls['city'].enable();
          this.getCity(value);
          // this.models = ... // here you add models to variable models based on selected make
        });
        this.educationForm = this._formBuilder.group({
          licence: [this.doctor && this.doctor.licence || '', [Validators.required]],
          // degree: [this.doctor && this.doctor.degree || '', [Validators.required]],
          // institution_name: [this.doctor && this.doctor.institution_name || '', [Validators.required, Validators.minLength(2)]],
          year_of_experience: [this.doctor && this.doctor.year_of_experience || '', [Validators.required, Validators.pattern('[0-9]+')]],
          // year: [this.doctor && this.doctor.year || '', [Validators.required, Validators.pattern('[0-9]+')]],
          national_licence: [this.doctor && this.doctor.national_licence || '', [Validators.required]],
          education_details:  this._formBuilder.array([])
           //  licence: [this.doctor && this.doctor.licence || '', [Validators.required]],
           // // degree: [this.doctor && this.doctor.degree || ''],
           //  //institution_name: [this.doctor && this.doctor.institution_name || ''],
           //  year_of_experience: [this.doctor && this.doctor.year_of_experience || ''],
           // // year: [this.doctor && this.doctor.year || '', [Validators.required]],
           //  national_licence:[this.doctor && this.doctor.year || ''],
           //  education_details:  this._formBuilder.array([])//[this.doctor && this.doctor.education_details || []]
        });
        this.personalValidation = new ValidateForm(this.doctorForm);
        this.educationValidation = new ValidateForm(this.educationForm);


        if(this.educationForm.value.education_details.length==0){
          if(this.doctor && this.doctor.education_details && this.doctor.education_details.trim()!=''){
            var datatemp =   JSON.parse(this.doctor.education_details);
            for(let i=0;i<datatemp.length;i++){
              this.employees().push(this.newEmployee(datatemp[i]));
            }
            this.educationForm.value.education_details =  [... JSON.parse(this.doctor.education_details)];
          }else{
            this.addEmployee();
          }
          console.log(this.educationForm.value.education_details)
          // this.addEmployee();
        }

        // this.createForm();
    }

  selectSpecialitiesOpened(): void {
    this.inputSearchSpecialities.nativeElement.focus();
  }

  selectSpecialitiesClosed(): void {
    this.inputSearchSpecialities.nativeElement.value = '';
    this.setSpecialities();
  }

  onKey({target: {value}}): void {
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => {
      this.setSpecialities(value);
    }, 400);
  }

  setSpecialities(value = ''): void {
    this._specialityService.searchSpeciality(0, 20, 'search=' + value).subscribe({
      next: (data) => {
        this.isLoading = false;

        if (data.status_code === 200) {
          this.specialites = data['result'].data;
          // this.totalItemsCount = data['result'].count;
          // this.loadedData.push(...data['result'].data);
        }
        if (data.status_code === 404) {
          this.specialites = [];
        }
      }
    })
  }

    employees(): FormArray {
      return this.educationForm.get('education_details') as FormArray;
    }

    newEmployee(data?): FormGroup {

      const educationForm = this._formBuilder.group({
        institution_name: [data && data.institution_name || '', [Validators.required, Validators.minLength(2)]],
        degree: [data && data.degree || '', [Validators.required]],
        year: [data && data.year || '', [Validators.required, Validators.pattern('[0-9]+')]],
      });



      return educationForm;
    }
    addEmployee() {
      console.log('Line no 179', this.educationForm);

      this.employees().push(this.newEmployee());

      console.log('[doctors-form.component.ts || Line no. 182 ....]', this.educationForm.get('education_details'))

    }

    removeEmployee(empIndex: number) {

      this.employees().removeAt(empIndex);
    }
   
    next(): void{
        
        this.selectedTabIndex = 1;
    }

    onIndexChange(index): void {
      this.selectedTabIndex = index;
    }
    // /**
    //  * Create Doctor form
    //  *
    //  * @returns
    //  */
    // createForm(): void {
    //     this.doctorForm = this._formBuilder.group({
    //         uuid: [this.doctor && this.doctor.uuid || ''],
    //         email: [this.doctor && this.doctor.email || '', [Validators.required]],
    //         title: [this.doctor && this.doctor.	title || ''],
    //         first_name: [this.doctor && this.doctor.first_name || '', [Validators.required]],
    //         middle_name: [this.doctor && this.doctor.middle_name || ''],
    //         last_name: [this.doctor && this.doctor.last_name || ''],
    //         phone: [this.doctor && this.doctor.phone || '', [Validators.required]],
    //         address: [this.doctor && this.doctor.address || '', [Validators.required]],
    //         language: [this.doctor && this.doctor.language || '', [Validators.required]],
    //         gender: [this.doctor && this.doctor.gender && this.doctor.gender.toLowerCase() || '', [Validators.required]],
    //         speciality: [this.doctor && parseInt(this.doctor.speciality) || '', [Validators.required]],
    //         // licence: [this.doctor && this.doctor.licence || '', [Validators.required]],
    //         dateOfBirth: [this.doctor && this.doctor.dateOfBirth || '', [Validators.required]],
    //         city: [this.doctor && this.doctor.city || '', [Validators.required]],
    //         state: [this.doctor && this.doctor.state || '', [Validators.required]],
    //         zip_code: [this.doctor && this.doctor.zip_code || '', [Validators.required]],
    //         bio: [this.doctor && this.doctor.bio || '', [Validators.required]],
    //         // education: [this.doctor && this.doctor.education || ''],
    //     });
    // }

    getState(): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._doctorService.getState();
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                // this.specialityCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                // 
                this.state = data;
                // console.log(this.specialites);
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }

    getCity(state): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._doctorService.getCity(state);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                // this.specialityCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }
            })
        ).subscribe(
            (data) => {
                // set response data
                // 
                this.city = data;
                // console.log(this.specialites);
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }
    /**
     * Create New Doctor
     * return {Void}
     */
    addDoctor(button): void {
        
        // eslint-disable-next-line no-
        // ;
        this.isLoading = true;
        button.target.disabled = true;
        const data = this.doctorForm.value;
        // console.log(data);
        data.year_of_experience = this.educationForm.value.year_of_experience;
        // data.year=this.educationForm.value.year;
        // data.degree = this.educationForm.value.degree;
        data.licence = this.educationForm.value.licence;
        // data.institution_name = this.educationForm.value.institution_name;
        data.national_licence = this.educationForm.value.national_licence;
        data.education_details = JSON.stringify(this.educationForm.value.education_details);
        this._doctorService.addDoctor(data).subscribe(
            (data) => {
                if(data.status_code == 201){
                  this._globalService.showMessage('New doctor added');
                    // this._matSnackBar.open('New doctor added', 'OK', this._globalService._matSnackBarConfig);
                    this._router.navigate(['/doctors']);
                }else{
                  this._globalService.showError(data.status_message);
                    // this._matSnackBar.open(data.status_message, 'OK', this._globalService._matSnackBarConfig);
                }
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);

            },
            (err) => {
                this._showErrorService.showError(err);
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);
            }
        );
    }

    /**
     * Get Doctors List
     * return {Void}
     */
    // getSpeciality(): void {
    //     // eslint-disable-next-line no-
    //     ;
    //     this.isLoading = true;
    // //    button.target.disabled = true;
    //  //   const data = this.doctorForm.value;
    //     // console.log(data);
    //     this._doctorService.getSpeciality().subscribe(
    //         () => {
    //             // this._matSnackBar.open('New doctor added', 'OK', this._globalService._matSnackBarConfig);
    //             // this._router.navigate(['/doctors']);
    //             setTimeout(() => {
    //                 this.isLoading = false;
    //                 // button.target.disabled = false;
    //             }, 500);

    //         },
    //         (err) => {
    //             
    //             this._showErrorService.showError(err);
    //             setTimeout(() => {
    //                 this.isLoading = false;
    //                 // button.target.disabled = false;
    //             }, 500);
    //         }
    //     );
    // }

    getSpecialityList(): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._doctorService.getSpeciality(params.page, params.limit);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                this.specialityCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                // 
                this.specialites = data;
                // console.log(this.specialites);
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }



    /**
     * Update Doctor
     * return{void}
     */
    saveDoctor(button): void {
        this.isLoading = true;
        button.target.disabled = true;

        const data = this.doctorForm.value;
        data.year_of_experience = this.educationForm.value.year_of_experience;
        // data.year=this.educationForm.value.year;
        // data.degree = this.educationForm.value.degree;
        data.licence = this.educationForm.value.licence;
        data.institution_name = this.educationForm.value.institution_name;
        data.national_licence = this.educationForm.value.national_licence;
        // data.education_details = this.educationForm.value.education_details;
        data.education_details = JSON.stringify(this.educationForm.value.education_details);
        // data.name  = data.first_name;
        // data.first_last_name  = data.last_name;
        this._doctorService.updateDoctor(data).subscribe(
            (data) => {
                if(data.status_code==200){
                  this._globalService.showMessage('Doctor Updated Successfully');
                    // this._matSnackBar.open('Doctor Updated Successfully', 'OK', this._globalService._matSnackBarConfig);
                    this._router.navigate(['/doctors']);
                }else{
                  this._globalService.showError(data.status_message)
                    // this._matSnackBar.open(data.status_message, 'OK', this._globalService._matSnackBarConfig);
                }
               
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);
            },
            (err) => {
                this._showErrorService.showError(err);
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);
            });
    }


}


/*
* Navigation Toggle button Removed
* Navigation Image Logo size Reduced
* Doctor dashboard Good morning text and date Dynamics
* Doctor form fixed
* Toolbar, Doctor dashboard name word capitalized
*  */
