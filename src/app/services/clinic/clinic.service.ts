import { Injectable } from '@angular/core';
import { AppUrlsService } from '../../core/app-urls/app-urls.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { handleError } from '../error-handler/error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class ClinicService {
  _clinicData: any;
  constructor(public _httpClient: HttpClient,
    private _appUrlService: AppUrlsService) { }
  /**
       * Get Doctor
       *
       * @returns
       */
  getclinics(page?, limit?): Observable<any> {
    return this._httpClient.get(this._appUrlService.clinic(page, limit)).pipe(
      catchError(handleError)
    );
  }
  /**
   * Search Clinic
   *
   * @returns
   */
  searchClinic(page, limit, queryField): Observable<any> {
    return this._httpClient.get(this._appUrlService.searchClinic(page, limit, queryField)).pipe(
      catchError(handleError)
    );
  }

  /**
   * ADD Doctor
   * @returns {observable<any>}
   */
  addClinic(data): Observable<any> {
    return this._httpClient.post(this._appUrlService.addClinic(), data).pipe(
      catchError(handleError)
    );
  }

  /**
   * Update Doctor
   * @returns {observable<any}
   */
  updateClinic(data): Observable<any> {
    const clinicId = data.id;
    // delete data['_id'];
    return this._httpClient.put(this._appUrlService.hospitalId(clinicId), data).pipe(
      catchError(handleError)
    );
  }
  /**
 * Get Dashboard
 * @returns {observable<any}
 */
  getDashboardCount(type): Observable<any> {
    return this._httpClient.get(this._appUrlService.getDashboardCount(type), ).pipe(
      catchError(handleError)
    );
  }
  /**
   * Update Doctor
   * @returns {observable<any}
   */
  deleteClinic(clinicId): Observable<any> {
    // delete data['_id'];
    return this._httpClient.delete(this._appUrlService.deleteCamp(clinicId)).pipe(
      catchError(handleError)
    );
  }
}
