/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';

let curUser = {users_id: undefined};
try {
    curUser = JSON.parse(window.localStorage.getItem('loggedInUser'));
} catch(e) {
    console.log(e);
}
export const defaultNavigation: FuseNavigationItem[] = [
    {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'basic',
        icon: 'mat_outline:dashboard',
        link: '/dashboard',
        action: 'representative,admin,doctor'
    },
    {
        id: 'clinic',
        title: 'Clinics',
        type: 'basic',
        icon: 'local_hotel',
        link: '/clinic',
        action: 'admin'
    },
    {
        id: 'agent',
        title: 'Representatives',
        type: 'basic',
        icon: 'person_pin',
        link: '/agent',
        action: 'admin'
    },
    {
        id: 'doctors',
        title: 'Doctors',
        type: 'basic',
        icon: 'iconsmind:doctor',
        link: '/doctors',
        action: 'admin,representative'
    },
    {
        id: 'patient',
        title: 'Patients',
        type: 'basic',
        icon: 'heroicons_outline:user',
        link: '/patients',
        action: 'admin,doctor'
    },
    {
        id: 'register',
        title: 'Register Patient',
        type: 'basic',
        icon: 'heroicons_outline:user',
        link: '/patients/register',
        action: 'representative'
    },
    {
        id: 'set-availability',
        title: 'Set Availability',
        type: 'basic',
        icon: 'schedule',
        link: `/set-availability`,
        action: 'doctor'
    },
    {
        id: 'psearch',
        title: 'Search Patient',
        type: 'basic',
        icon: 'heroicons_outline:user',
        link: '/patients/search/list',
        action: 'representative'
    },
    {
        id: 'appointment',
        title: 'Appointments',
        type: 'basic',
        icon: 'iconsmind:calendar_clock',
        link: '/appointments',
        action: 'representative,admin,doctor'
    },
    {
        id: 'call-queue',
        title: 'Call Queue',
        type: 'basic',
        icon: 'queue',
        link: '/call-queue',
        action: 'doctor'
    },
    {
        id: 'rating-review',
        title: 'Rating & Review',
        type: 'basic',
        icon: 'rate_review',
        link: '/rating-review',
        action: 'admin,doctor'
    },
    //    {
    //         id: 'prescription',
    //         title: 'Prescription',
    //         type: 'basic',
    //         icon: 'iconsmind:notepad',
    //         link: '/doctors/create/prescription',
    //         action: 'doctor'
    //     },
    {
        id: 'speciality',
        title: 'Specialities',
        type: 'basic',
        icon: 'mat_solid:folder_special',
        link: '/speciality',
        action: 'admin'
    },
    {
        id: 'call-logs',
        title: 'Call Logs',
        type: 'basic',
        icon: 'mat_outline:add_ic_call',
        link: '/call-logs',
        action: 'representative,admin,doctor'
    },
    {
        id: 'change-password',
        title: 'Change Password',
        type: 'basic',
        icon: 'iconsmind:password_field',
        link: '/reset-password',
        action: 'representative,admin,doctor'
    },
    {
        id: 'logout',
        title: 'Logout',
        type: 'basic',
        icon: 'heroicons_outline:logout',
        link: '/sign-in',
        action: 'representative,admin,doctor'
    }

];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
