import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { AppointmentService } from '../../../../services/appointment/appointment.service';
import { merge, Observable } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { Pagination } from '../../../../shared/classes/pagination.class';
import { GlobalService } from '../../../../services/global/global.service';
import {ShowErrorService} from '../../../../services/show-error/show-error.service';
import { FormControl, FormGroup } from '@angular/forms';
import moment from 'moment';
import {Router} from '@angular/router';
import {CallLogsService} from '../../../../services/call-logs/call-logs.service';
import {TableExportToExcelDirective} from '../../../../shared/directives/table-export-to-excel.directive';

@Component({
    selector: 'app-call-logs-list',
    templateUrl: './call-logs-list.component.html',
    styleUrls: ['./call-logs-list.component.scss']
})
export class CallLogsListComponent extends Pagination implements OnInit, AfterViewInit {
    public httpReq: (page: number, limit: number, queryField: string) => Observable<any>;

    callLogsCount: number = 0;
    callLogsTableColumns: string[] = ['p_name', 'd_name', 'duration', 'date', 'action'];
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    @ViewChild(TableExportToExcelDirective) table: TableExportToExcelDirective;
    callLogs: Array<any> = [];
    filterForm: FormGroup;

    constructor(
      private _appointmentService: AppointmentService,
      public _globalService: GlobalService,
      private _router: Router,
      private _showErrorService: ShowErrorService,
      private _callLogsService: CallLogsService
    ) {
        super();
        const role = localStorage.getItem('role');
        const curUser = JSON.parse(localStorage.getItem('loggedInUser'));
        let roleQuery = '';
        if(role && role === 'representative') {
            roleQuery = 'agent_id=' + curUser.users_id;
        }
        if(role && role === 'doctor') {
            roleQuery = 'doctor_id=' + curUser.users_id;
        }
        this.httpReq = (page, limit, queryField): Observable<any> => this._globalService.searchCallLogs(page, limit, queryField + roleQuery);
    }

    ngOnInit(): void {
        this.callLogsCount = 1;
        this.filterForm = new FormGroup({
            search: new FormControl(null, []),
            date: new FormControl(null, []),
        });
    }

    /**
     * After View Init
     */
    ngAfterViewInit(): void {
        this.fetchForFirstTime().then();
    }

    getDuration(start, end): string {
        const duration = moment.duration(moment(end).diff(moment(start)));
        const hours = duration.get('hours');
        const seconds = duration.get('seconds');
        const minutes = duration.get('minutes');
        return `${hours ? hours + 'h ' : ''} ${minutes ? minutes + 'm ' : ''} ${seconds ? seconds + 's' : ''}`;
    }

    resetSearch(e): void {
        e.stopPropagation();
        this.filterForm.reset();
        this.isSearchedList = false;
        this.handleSearchClick(this.filterForm).then();
    }


    /**
     * Get Appointments List
     * return {Void}
     */

    getAppointmentsList(): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._appointmentService.getPastAppointments(params.page, params.limit, JSON.parse(localStorage.getItem('loggedInUser')).hospital_id);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                console.log('[call-logs-list.component.ts || Line no. 61 ....]', data);
                this.callLogsCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                this.callLogs = data;
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }

    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any {
        return item.id || index;
    }

    preview(callLog): void {
        this._callLogsService._callLogs = callLog;
        localStorage.setItem('listItem', JSON.stringify(callLog));
        // this._doctorService._doctorData = doctor;
        this._router.navigate([`/call-logs/preview/${callLog.id}/view`]);
    }

}
