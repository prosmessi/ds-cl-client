import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { browserRefresh } from '../../../../app.component';
import moment from 'moment';
import { CallLogsService } from '../../../../services/call-logs/call-logs.service';
import { GlobalService } from '../../../../services/global/global.service';

@Component({
  selector: 'app-call-logs-preview',
  templateUrl: './call-logs-preview.component.html',
  styleUrls: ['./call-logs-preview.component.scss']
})
export class CallLogsPreviewComponent implements OnInit {
  callLogId: string;
  isLoading = false;
  callLog;
  constructor(
    private _activatedRoute: ActivatedRoute,
    public _globalService: GlobalService,
    private _callLogsService: CallLogsService
  ) { }

  ngOnInit(): void {
    this.callLogId = this._activatedRoute.snapshot.paramMap.get('callLogId') || '';

    // this.callLog = this._doctorService._doctorData;
    this.callLog = this._callLogsService._callLogs;
    if (browserRefresh && JSON.parse(localStorage.getItem('listItem'))) {
      this.callLog = JSON.parse(localStorage.getItem('listItem'));
    }
    console.log(this.callLog);
    this.callLog.allergies = this.callLog.allergies ? JSON.parse(this.callLog.allergies) : [];
    this.callLog.medications = this.callLog.medications ? JSON.parse(this.callLog.medications) : [];
  }

  getDuration(start, end): string {
    const duration = moment.duration(moment(end).diff(moment(start)));
    const hours = duration.get('hours');
    const seconds = duration.get('seconds');
    const minutes = duration.get('minutes');
    return `${hours ? hours + ' hours ' : ''} ${minutes ? minutes + ' minutes ' : ''} ${seconds ? seconds + ' seconds' : ''}`;
  }

}
