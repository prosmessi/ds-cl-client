import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DoctorService} from '../../../../services/doctor/doctor.service';
import {GlobalService} from '../../../../services/global/global.service';
import {ShowErrorService} from '../../../../services/show-error/show-error.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {map, startWith, switchMap} from 'rxjs/operators';
import {browserRefresh} from "../../../../app.component";
import {merge} from 'rxjs';
import * as moment from 'moment';


@Component({
    selector: 'app-availability-form',
    templateUrl: './availability-form.component.html',
    styleUrls: ['./availability-form.component.scss']
})
export class AvailabilityFormComponent implements OnInit {
  durations: any[] = [
    {
      key : 5,
      value : "5 Min"
    },
    {
      key : 10,
      value : "10 Min"
    },
    {
      key : 15,
      value : "15 Min"
    },
    {
        key : 20,
        value : "20 Min"
      },
      {
        key : 25,
        value : "25 Min"
      },
      {
        key : 30,
        value : "30 Min"
      },
      {
        key : 35,
        value : "35 Min"
      },
      {
        key : 40,
        value : "40 Min"
      },
      {
        key : 45,
        value : "45 Min"
      },
      {
        key : 50,
        value : "50 Min"
      },
      {
        key : 55,
        value : "55 Min"
      },
      {
        key : 60,
        value : "60 Min"
      }
  ];
  duration : number;
  days: any[] = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  timeslot: any[];
  timeslot1: any[];
  showToolTip:boolean = true;
  workingHours: any = [
    {
    dayname: 'Sunday',
    daystatus: false,
    session: [{
      fromTime: '',
      toTime: ''
    }]
  }, {
    dayname: 'Monday',
    daystatus: false,
    session: [{
      fromTime: '',
      toTime: ''
    }]
  }, {
    dayname: 'Tuesday',
    daystatus: false,
    session: [{
      fromTime: '',
      toTime: ''
    }]
  }, {
    dayname: 'Wednesday',
    daystatus: false,
    session: [{
      fromTime: '',
      toTime: ''
    }]
  }, {
    dayname: 'Thursday',
    daystatus: false,
    session: [{
      fromTime: '',
      toTime: ''
    }]
  }, {
    dayname: 'Friday',
    daystatus: false,
    session: [{
      fromTime: '',
      toTime: ''
    }]
  }, {
    dayname: 'Saturday',
    daystatus: false,
    session: [{
      fromTime: '',
      toTime: ''
    }]
  }];
  start = "05:00";
  end = "23:00";
  disabled = false;
  checked = false; 
  isLoading: boolean;
  uuid:any;
  role:any;
  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _formBuilder: FormBuilder,
    private _doctorService: DoctorService,
    private _globalService: GlobalService,
    private _showErrorService: ShowErrorService,
    private _matSnackBar: MatSnackBar
  ) {
   }
  ngOnInit(): void {
    console.log('[availability-form.component.ts || Line no. 143 ....]', this._activatedRoute.snapshot.routeConfig.path);


      if(this._activatedRoute.snapshot.routeConfig.path === 'set-availability') {
        var loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
        this.uuid = loggedInUser.uuid;
        this.role = loggedInUser.user_type_id;
      } else {
        loggedInUser = JSON.parse(localStorage.getItem('listItem'));
        this.uuid = loggedInUser.uuid;
        this.role = loggedInUser.user_type_id;
      }

      if(loggedInUser.video_consultation && loggedInUser.video_consultation!=""){
        var data = JSON.parse(loggedInUser.video_consultation)
        this.duration = data.videoCosulationduration;
        this.durationchange();
        this.workingHours =data.videoCosulationworkingHours;
      }
      // this.durationchange();
  }
  tConvert(time) {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) { // If time format correct
      time = time.slice(1);  // Remove full string match value
      time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    
    return time.join(''); // return adjusted time or original string
  }
  onToggleDay(event, day) {
    console.log(event, this.workingHours[day]);
    if(event.checked && 
        this.workingHours[day].session.length === 1 && 
        !this.workingHours[day].session[0].fromTime.trim() && 
        !this.workingHours[day].session[0].toTime.trim()) {
          this.workingHours[day].session[0].fromTime = ""
          this.workingHours[day].session[0].toTime = ""
    }
  }
  onFromTimeChange(event, day, index) {

    if (this.workingHours[day].session[index].toTime && this.getTwentyFourHourTime(event.source.value) >= this.getTwentyFourHourTime(this.workingHours[day].session[index].toTime)) {
      event.source.value = null
    } else {
      this.workingHours[day].session.some((data1, i) => {
        if (i != index) {
          if (data1.fromTime && data1.toTime && this.getTwentyFourHourTime(data1.fromTime) <= this.getTwentyFourHourTime(event.source.value) &&
          this.getTwentyFourHourTime(data1.toTime) > this.getTwentyFourHourTime(event.source.value)) {
            event.source.value = null
            this._globalService.showError('Please select Proper timing for set availability');
            // this._matSnackBar.open('Please select Proper timing for set availability', 'OK', this._globalService._matSnackBarConfig);
            return true;
          } else {
          }
        }
      })
    }
  }
  onToTimeChange(event, day, index) {

    if (this.workingHours[day].session[index].fromTime && this.getTwentyFourHourTime(event.source.value) <= this.getTwentyFourHourTime(this.workingHours[day].session[index].fromTime)) {
      event.source.value = null
      this._globalService.showError('Please select Proper timing for set availability');
      // this._matSnackBar.open('Please select Proper timing for set availability', 'OK', this._globalService._matSnackBarConfig);
    } else {
      this.workingHours[day].session.some((data1, i) => {
        if (i != index) {
          if (data1.fromTime && data1.toTime && this.getTwentyFourHourTime(data1.fromTime) < this.getTwentyFourHourTime(event.source.value) &&
          this.getTwentyFourHourTime(data1.toTime) >= this.getTwentyFourHourTime(event.source.value)) {
            event.source.value = null
            this._globalService.showError('Please select Proper timing for set availability');
            // this._matSnackBar.open('Please select Proper timing for set availability', 'OK', this._globalService._matSnackBarConfig);
            return true;
          } else if (this.workingHours[day].session[index].fromTime && this.getTwentyFourHourTime(data1.fromTime) > this.getTwentyFourHourTime(this.workingHours[day].session[index].fromTime)  &&
          this.getTwentyFourHourTime(data1.fromTime)  < this.getTwentyFourHourTime(event.source.value) ) {
            event.source.value = null
            this._globalService.showError('Please select Proper timing for set availability');
            // this._matSnackBar.open('Please select Proper timing for set availability', 'OK', this._globalService._matSnackBarConfig);
            return true;
          } else {
          }
        }
      })
    }
  }
  addMore(key) {
    if (this.workingHours[key].session.length < 3) {
      this.workingHours[key].session.push({
        fromTime: '',
        toTime: ''
      });
    }
  }
  removeAddress(key, length) {
    this.workingHours[key].session.splice(length, 1);
  }
  getTimeStops(start, end, interval) {
    var startTime = moment(start, 'HH:mm');
    var endTime = moment(end, 'HH:mm');
    if (endTime.isBefore(startTime)) {
      endTime.add(1, 'day');
    }
    var timeStops = [];
    while (startTime <= endTime) {
      console.log(moment(startTime).format('HH:mm A'));
      timeStops.push(this.tConvert(moment(startTime).format('HH:mm')))// != '00:00 AM'?moment(startTime).format('HH:mm A'):'12:00 AM');
      startTime.add(interval, 'minutes');
    }
    return timeStops;
  }
  durationchange(){
    this.showToolTip = false;
    console.log(this.duration);
    var startTime = moment(this.start, 'HH:mm');
    var endTime = moment(this.end, 'HH:mm');
    startTime.add(this.duration, 'minutes').format('HH:mm');
    this.timeslot = this.getTimeStops(this.start, this.end, this.duration);
    this.timeslot1 = this.getTimeStops(startTime, this.end, this.duration);

    for(let i=0;i<this.workingHours.length;i++){
      this.workingHours[i].session=[{fromTime: '',toTime: ''}];
    }
  }

  getTwentyFourHourTime(amPmString) {
    // amPmString = amPmString.split(' ')[0];
    var d = new Date("1/1/2013 " + amPmString);
    console.log(d.getHours() + ':' + (d.getMinutes() == 0 ? '00' : d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes()))
    return (d.getHours()<10?'0'+d.getHours():d.getHours() ) + ':' + (d.getMinutes() == 0 ? '00' : d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes());
  }

    /**
     * Create New Doctor
     * return {Void} /doctor​/update​/availability​/:uuid
     */
     setAvailability(): void {
      var datastring = {
        videoCosulation: true,
        videoCosulationfee: "100",
        videoCosulationduration: this.duration,
        videoCosulationnoofdays: "2",
        videoCosulationaddCalender: "",
        videoCosulationfreeFollowUp: true,
        videoCosulationworkingHours:  this.workingHours
      }
      var data = {
        video_consultation: JSON.stringify(datastring),
        in_clinic_consultation: JSON.stringify(datastring),
        time_Off: JSON.stringify(datastring),
        role: JSON.stringify(this.role),
    };
      // console.log(data);

      this._doctorService.addavailability(this.uuid,data).subscribe(
          async (result) => {
              if(result.status_code == 200){
                console.log('[availability-form.component.ts || Line no. 303 ....]', result);
                // If doctor is updating his own availability
                if(this._activatedRoute.snapshot.routeConfig.path === 'set-availability') {
                  await new Promise((res, rej) => {
                    setTimeout(res, 1200);
                  });
                  const item = JSON.parse(localStorage.getItem('loggedInUser'));
                  item.video_consultation = result.result[0].video_consultation;
                  item.time_Off = result.result[0].time_Off;
                  item.in_clinic_consultation = result.result[0].in_clinic_consultation;
                  localStorage.setItem('loggedInUser', JSON.stringify(item));
                  this._globalService.showMessage('Availability updated successfully');
                  this._router.navigate(['/dashboard']);
                  return;
                }
                // var loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
                // this._globalService.fetchUserById(loggedInUser.users_id).subscribe(result => {
                //   if(result.status_code == 200) {
                //   }
                // })
              // this._matSnackBar.open('Availability updated successfully', 'OK', this._globalService._matSnackBarConfig);
              this._router.navigate(['/doctors']);
              }else{
                this._globalService.showError(result.status_message)
                // this._matSnackBar.open(result.status_message, 'OK', this._globalService._matSnackBarConfig);
              }
              setTimeout(() => {
                  this.isLoading = false;
              }, 500);
          },
          (err) => {
              this._showErrorService.showError(err);
              setTimeout(() => {
                  this.isLoading = false;
              }, 500);
          }
      );
  }
}
