import {Component, OnDestroy, OnInit} from '@angular/core';
import {browserRefresh} from "../../../../app.component";
import {ActivatedRoute, Router} from "@angular/router";
import {SpecialityService} from "../../../../services/speciality/speciality.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {GlobalService} from "../../../../services/global/global.service";
import {ShowErrorService} from "../../../../services/show-error/show-error.service";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
    selector: 'app-speciality-form',
    templateUrl: './speciality-form.component.html',
    styleUrls: ['./speciality-form.component.scss']
})
export class SpecialityFormComponent implements OnInit, OnDestroy {

    pageType: string;
    specialityId: string;
    speciality: any;
    specialityForm: FormGroup;
    isLoading: boolean = false;

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _specialityService: SpecialityService,
        private _formBuilder: FormBuilder,
        private _globalService: GlobalService,
        private _showErrorService: ShowErrorService,
        private _matSnackBar: MatSnackBar,
        private _router: Router
    ) {
    }

    ngOnInit(): void {

        this.pageType = this._activatedRoute.snapshot.paramMap.get('handle') || '';
        this.specialityId = this._activatedRoute.snapshot.paramMap.get('specialityId') || '';

        this.speciality = this._specialityService._specialityData;
        if (browserRefresh && JSON.parse(localStorage.getItem('listItem'))) {
            this.speciality = JSON.parse(localStorage.getItem('listItem'));
        }
        this.createForm();
    }


    /**
     * On destroy
     */

    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._specialityService._specialityData = {}
    }

    /**
     * Create Doctor form
     *
     * @returns
     */
    createForm(): void {
        this.specialityForm = this._formBuilder.group({
            id: [ ''],
            name: [ '', [Validators.required]]
        });
    }

    /**
     * Create New Doctor
     * return {Void}
     */
    addSpeciality(button): void {
        this.isLoading = true;
        button.target.disabled = true;
        const data = this.specialityForm.value;
        // console.log(data);
        this._specialityService.addSpeciality(data).subscribe(
            () => {
                this._globalService.showMessage('New Speciality added');
                // this._matSnackBar.open('New Speciality added', 'OK', this._globalService._matSnackBarConfig);
                this._router.navigate(['/speciality']);
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);

            },
            (err) => {
                this._showErrorService.showError(err);
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);
            }
        );
    }

    
}
