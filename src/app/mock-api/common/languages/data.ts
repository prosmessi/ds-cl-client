// Assamese, Bengali, Gujarati, Hindi, Kannada, Kashmiri, Konkani, Malayalam, Manipuri, Marathi, Nepali, Oriya, Punjabi, Sanskrit, Sindhi, Tamil, Telugu, Urdu, Bodo, Santhali, Maithili and Dogri

const languagesArr = [
	{
		id         : '1',
		label      : 'Assamese'
	},
	{
		id         : '2',
		label      : 'Bengali'
	},
	{
		id         : '3',
		label      : 'Gujarati'
	},
	{
		id         : '4',
		label      : 'Hindi'
	},
	{
		id         : '5',
		label      : 'Kannada'
	},
	{
		id         : '6',
		label      : 'Kashmiri'
	},
	{
		id         : '7',
		label      : 'Konkani'
	},
	{
		id         : '8',
		label      : 'Malayalam'
	},
	{
		id         : '9',
		label      : 'Manipuri'
	},
	{
		id         : '10',
		label      : 'Marathi'
	},
	{
		id         : '11',
		label      : 'Nepali'
	},
	{
		id         : '12',
		label      : 'Oriya'
	},
	{
		id         : '13',
		label      : 'Punjabi'
	},
	{
		id         : '14',
		label      : 'Sanskrit'
	},
	{
		id         : '15',
		label      : 'Sindhi'
	},
	{
		id         : '16',
		label      : 'Tamil'
	},
	{
		id         : '17',
		label      : 'Telugu'
	},
	{
		id         : '18',
		label      : 'Urdu'
	},
	{
		id         : '19',
		label      : 'Bodo'
	},
	{
		id         : '20',
		label      : 'Santhali'
	},
	{
		id         : '21',
		label      : 'Maithili'
	},
	{
		id         : '22',
		label      : 'Dogri'
	},
	{
		id         : '23',
		label      : 'English'
	},
];


export const languages = languagesArr.sort((a, b) => {
	if(a.label === 'English' || a.label === 'Hindi') return -1;
	if(b.label === 'English' || b.label === 'Hindi') return 1;
	if(a.label < b.label) return -1;
	if(a.label > b.label) return 1;
	return 0
});
