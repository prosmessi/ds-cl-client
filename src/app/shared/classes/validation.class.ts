import {FormGroup} from '@angular/forms';

interface IDataItem {
	name: string;
}

export interface IValidationMessageObj {
	ifPattern: (msg: string) => string;
	message: string;
}

export abstract class ValidationBase {

	public abstract form: FormGroup;

	private getCondition(formControlName: string, isTouched?: boolean): boolean {
		if(isTouched) return this.form.get(formControlName).status === 'INVALID' && this.form.get(formControlName).touched;
		return this.form.get(formControlName).status === 'INVALID' && (this.form.get(formControlName).dirty || this.form.get(formControlName).touched);
	}

	getValidationMessage(formControlName: string): IValidationMessageObj {
		let isPattern = false;
		// If the condition is not true for the control name
		// then sending the message null
		if(!this.getCondition(formControlName)) {
			return {
				ifPattern: (msg?: string): string => {
					if (isPattern && msg) {return null;}
					return null;
				},
				message: null
			};
		}


		let message = 'Invalid Field';
		if(this.form.get(formControlName).errors?.required) {
			message = 'Required field';
		}
		if(this.form.get(formControlName).errors?.maxlength) {
			message = null;
			if(this.getCondition(formControlName, true)) {
				message = 'Seems too big';
			}
		}
		if(this.form.get(formControlName).errors?.minlength) {
			message = null;
			if(this.getCondition(formControlName, true)) {
				message = 'Seems too short';
			}
		}
		if(this.form.get(formControlName).errors?.email) {
			message = 'Invalid email';
		}
		if(this.form.get(formControlName).errors?.pattern) {
			// TODO: Assuming the pattern is only for checking number [Temporary]
			message = 'Must only contain digits';
			isPattern = true;
		}

		return {
			ifPattern: (msg?: string): string => {
				if (isPattern && msg) {return msg;}
				return message;
			},
			message
		};
	}
}

export class ValidateForm extends ValidationBase {

	public form: FormGroup;

	// getSelected(type: string): void;
	// getSelected(type: string, one: true): IDataItem;
	// getSelected(type: string, one: false): IDataItem[];

	constructor(form: FormGroup) {
		super();
		this.form = form;
	}

}
