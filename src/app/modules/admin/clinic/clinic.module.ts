import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClinicListComponent} from './clinic-list/clinic-list.component';
import {ClinicFormComponent} from './clinic-form/clinic-form.component';
import {ClinicPreviewComponent} from './clinic-preview/clinic-preview.component';
import {Route, RouterModule} from '@angular/router';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {CdkScrollableModule} from '@angular/cdk/scrolling';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {DoctorService} from '../../../services/doctor/doctor.service';
import {SharedModule} from '../../../shared/shared.module';

const routes: Route[] = [
    {
        path: '',
        component: ClinicListComponent
    },
    {
        path: ':handle',
        component: ClinicFormComponent,

    },
    {
        path: ':handle/:doctorId',
        component: ClinicFormComponent,

    },
    {
        path: ':preview/:doctorId/view',
        component: ClinicPreviewComponent,

    },
    {
        path: '**',
        component: ClinicListComponent,
    }
];

@NgModule({
    declarations: [
        ClinicListComponent,
        ClinicFormComponent,
        ClinicPreviewComponent
    ],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
		MatProgressBarModule,
		MatSortModule,
		MatTableModule,
		MatIconModule,
		MatButtonModule,
		CdkScrollableModule,
		MatFormFieldModule,
		MatInputModule,
		MatPaginatorModule,
		MatTooltipModule,
		ReactiveFormsModule,
		MatSelectModule,
		SharedModule
	],
    providers: [
        DoctorService
    ]
})
export class ClinicModule {
}
