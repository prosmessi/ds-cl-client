import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorSearchListComponent } from './doctor-search-list.component';

describe('DoctorSearchListComponent', () => {
  let component: DoctorSearchListComponent;
  let fixture: ComponentFixture<DoctorSearchListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorSearchListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorSearchListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
