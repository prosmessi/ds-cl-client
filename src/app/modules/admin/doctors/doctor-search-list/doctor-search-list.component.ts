import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {merge, Observable} from 'rxjs';
import {map, startWith, switchMap} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DoctorService} from '../../../../services/doctor/doctor.service';
import {Router} from '@angular/router';
import {MatPaginator} from '@angular/material/paginator';
import {Pagination} from '../../../../shared/classes/pagination.class';
import {GlobalService} from '../../../../services/global/global.service';
import {TableExportToExcelDirective} from '../../../../shared/directives/table-export-to-excel.directive';

@Component({
    selector: 'app-doctor-search-list',
    templateUrl: './doctor-search-list.component.html',
    styleUrls: ['./doctor-search-list.component.scss']
})
export class DoctorSearchListComponent extends Pagination implements OnInit, AfterViewInit {
    public httpReq: (page: number, limit: number, queryField: string) => Observable<any>;

    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    isLoading: boolean = false;
    searchForm: FormGroup;
    specialites: Array<any> = [];
    specialityCount: number = 0;
    doctorsCount: number = 0;
    doctorsTableColumns: string[] = ['name', 'emailPhone', 'speciality', 'experience', 'status', 'createdAt', 'action'];
    doctors: Array<any> = [];
    name: string = '';
    // doctorsCount: number = 0;
    role: string;

    constructor(
        private _doctorService: DoctorService,
        private _formBuilder: FormBuilder,
        private _router: Router,
        private _globalService: GlobalService
    ) {
        super();
        this.role = localStorage.getItem('role');
        this.httpReq = (page, limit, queryField): Observable<any> => this._globalService.getUserSearch(page, limit, queryField + 'role=doctor');
    }

    ngOnInit(): void {
        this.getSpecialityList();
        this.searchForm = this._formBuilder.group({
            search: '',
            speciality: 1
        })
        this.doctorsCount = 1;
    }
    /**
     * After View Init
     */
    ngAfterViewInit(): void {
        this.getDoctorList('1','');
        this.fetchForFirstTime().then();
    }


    getSpecialityList(): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
          startWith({}),
          switchMap(() => {
              this.isLoading = true;
              const params = {
                  page: 1,
                  limit: 20,
              };
              return this._doctorService.getSpeciality(params.page, params.limit);
          }),
          map((data) => {
              this.isLoading = false;
              // set pagination total count
              this.specialityCount = data['result'].length;

              // return response data
              if (data.status_code === 200) {
                  return data['result'];
              }


          })
        ).subscribe(
          (data) => {
              // set response data

              this.specialites = data;
              // console.log(this.specialites);
              /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
							 this.dataSource.sort = this.sort;*/
          },
          (err) => {
              // show the error
              // console.log('err: ', err);
          }
        );
    }

    /**
     * Get Appointments List
     * return {Void}
     */

    getDoctorList(spec,name): void {
        
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._doctorService.getdoctorSearch(params.page, params.limit,spec,name);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                this.doctorsCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }else{
                    return [];
                } 

            })
        ).subscribe(
            (data) => {
                // set response data
                this.doctors = data;
                
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                this.doctors =[];
                // show the error
                // console.log('err: ', err);
            }
        );
    }
    /**
     * Preview doctor's info
     *
     * @param doctor
     */
    previewDoctor(doctor): void {
        localStorage.setItem('listItem', JSON.stringify(doctor));
        this._doctorService._doctorData = doctor;
        this._router.navigate([`/doctors/preview/${doctor.users_id}/view`]);
    }

    gotoPatientDetails(data): void {
        
        localStorage.setItem('doctorItem', JSON.stringify(data));
     //   if(!data.is_available){
         //   this._router.navigate([`/patients/medical/details`]);
       // }else{
            this._router.navigate([`/appointments/time`]);
      //  }
        
     }

}
