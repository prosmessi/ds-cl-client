import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppUrlsService} from '../../core/app-urls/app-urls.service';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {handleError} from '../error-handler/error-handler.service';

@Injectable({
    providedIn: 'root'
})
export class AppointmentService {

    _data: any;

    constructor(
        private _httpClient: HttpClient,
        private _appUrlService: AppUrlsService
    ) {
    }

    /**
     * Get Doctor
     *
     * @returns
     */
    getAppointments(page?, limit?,hospitalId?): Observable<any> {
        return this._httpClient.get(this._appUrlService.appointments(page, limit,hospitalId)).pipe(
            catchError(handleError)
        );
    }

    /**
     * Get Doctor
     *
     * @returns
     */
    getPastAppointments(page?, limit?,hospitalId?): Observable<any> {
        return this._httpClient.get(this._appUrlService.pastAppointments(page, limit,hospitalId)).pipe(
            catchError(handleError)
        );
    }
    /**
     * Get Doctor
     *
     * @returns
     */
    searchAppointments(page?, limit?, queryField?): Observable<any> {
        return this._httpClient.get(this._appUrlService.searchAppointments(page, limit, queryField)).pipe(
          catchError(handleError)
        );
    }

    
    /**
     * /doctor​/callWaitingList​/:uuid
     *
     * @returns
     */
     callWaitingList​(uuid): Observable<any> {
        return this._httpClient.get(this._appUrlService.callWaitingList​(uuid)).pipe(
            catchError(handleError)
        );
    }

    /*
    * getRtcAccessToken
    * @returns
    */
    getRtcAccessToken(data): Observable<any> {
        return this._httpClient.post(this._appUrlService.getRtcAccessToken(), data).pipe(
            catchError(handleError)
        );
        // return this._httpClient.get(this._appUrlService.getRtcAccessToken(channelName,uid)).pipe(
        //     catchError(handleError)
        // );
    }

    //updateCallStatus
      updateCallStatus(data): Observable<any> {
        return this._httpClient.put(this._appUrlService.updateCallStatus(), data).pipe(
            catchError(handleError)
        );
        // return this._httpClient.get(this._appUrlService.getRtcAccessToken(channelName,uid)).pipe(
        //     catchError(handleError)
        // );
    }

    /**
     * Delete Doctor
     *
     * @returns
     */
    deleteAppointment(id): Observable<any> {
        return this._httpClient.delete(this._appUrlService.deleteAppointment(id)).pipe(
            catchError(handleError)
        );
    }

    /**
     * Delete Doctor
     *
     * @returns
     */
    appointmentMakeCall(data): Observable<any> {
        return this._httpClient.post(this._appUrlService.appointmentMakeCall(), data).pipe(
            catchError(handleError)
        );
    }

}
