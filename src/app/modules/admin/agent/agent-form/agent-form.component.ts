import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DoctorService} from '../../../../services/doctor/doctor.service';
import {GlobalService} from '../../../../services/global/global.service';
import {ShowErrorService} from '../../../../services/show-error/show-error.service';
import {ClinicService} from '../../../../services/clinic/clinic.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {map, startWith, switchMap} from 'rxjs/operators';
import {browserRefresh} from "../../../../app.component";
import {merge} from 'rxjs';
import { ValidationBase} from '../../../../shared/classes/validation.class';

@Component({
    selector: 'app-agent-form',
    templateUrl: './agent-form.component.html',
    styleUrls: ['./agent-form.component.scss']
})
export class AgentFormComponent extends ValidationBase implements OnInit {

    isLoading: boolean = false;
    pageType: string;
    doctorId: string;
    form: FormGroup;
    agent: any;
    state: Array<any> = [];
    city: Array<any> = [];
    clinic:Array<any> = [];
    camps: Array<any> = [];
    selectedTabIndex: number = 0;
    timer: any;

    @ViewChild('input') inputSearchCamps: ElementRef<HTMLInputElement>;
    // specialites: Array<any> = [];
    // specialityCount: number = 0;

    /**
     * Constructor
     */
    constructor(
        private _router: Router,
        private _activatedRoute: ActivatedRoute,
        private _formBuilder: FormBuilder,
        private _doctorService: DoctorService,
        private _globalService: GlobalService,
        private _showErrorService: ShowErrorService,
        private _matSnackBar: MatSnackBar,
        private _clinicService:ClinicService
    ) {
        super();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // this.getSpecialityList();
        this.getState();
        this.getClinic();
        this.setCamps();
        
        this.pageType = this._activatedRoute.snapshot.paramMap.get('handle') || '';
        this.doctorId = this._activatedRoute.snapshot.paramMap.get('doctorId') || '';

        this.agent = this._doctorService._doctorData;
        if(this.agent && this.agent.state && this.agent.state !=''){
            this.getCity(this.agent.state);
        }
        if (browserRefresh && JSON.parse(localStorage.getItem('listItem'))) {
            this.agent = JSON.parse(localStorage.getItem('listItem'));
            if(this.agent && this.agent.state && this.agent.state !=''){
                this.getCity(this.agent.state);
            }
        }
        if(this.pageType != 'edit'){
            this.agent = null;
        }
        this.form = this._formBuilder.group({
            uuid: [this.agent && this.agent.uuid || ''],
            email: [this.agent && this.agent.email || '', [Validators.required, Validators.email]],
            first_name: [this.agent && this.agent.first_name || '', [Validators.required, Validators.minLength(2)]],
            last_name: [this.agent && this.agent.last_name || '', [Validators.required, Validators.minLength(2)]],
            phone: [this.agent && this.agent.phone || '', [Validators.required, Validators.pattern('[0-9]+'), Validators.minLength(10), Validators.maxLength(11)]],
            address: [this.agent && this.agent.address1 || '', [Validators.required, Validators.minLength(5)]],
            city: [{value: this.agent && this.agent.city || '', disabled: !this.agent?.city}, [Validators.required]],
            hospital_id: [this.agent && this.agent.hospital_id || '', [Validators.required]],
            state: [this.agent && this.agent.state || '', [Validators.required]],
            gender: [this.agent && this.agent.gender || '', [Validators.required]],
            // speciality: [this.doctor && parseInt(this.doctor.speciality) || '', [Validators.required]],
            aadhaar_number: [this.agent && this.agent.aadhaar_number || '', [Validators.required, Validators.pattern('[0-9]+'), Validators.minLength(12), Validators.maxLength(12)]],
            // image: [''],
            // education: [this.doctor && this.doctor.education || ''],
            zip_code: [this.agent && this.agent.zip_code || '', [Validators.required, Validators.pattern('[0-9]+'), Validators.minLength(3), Validators.maxLength(15)]]

        });
        
        this.form.controls['state'].valueChanges.subscribe((value) => {
            console.log(value);
            this.form.controls['city'].enable();
            this.getCity(value);
            // this.models = ... // here you add models to variable models based on selected make
          });
        // this.createForm();
    }

    /**
     * Create Doctor form
     *
     * @returns
     */
    // createForm(): void {
    //     this.form = this._formBuilder.group({
    //         uuid: [this.agent && this.agent.uuid || ''],
    //         email: [this.agent && this.agent.email || '',[Validators.required]],
    //         first_name: [this.agent && this.agent.first_name || '',[Validators.required]],
    //         last_name: [this.agent && this.agent.last_name || '',[Validators.required]],
    //         phone: [this.agent && this.agent.phone || '',[Validators.required]],
    //         address: [this.agent && this.agent.address || '',[Validators.required]],
    //         city: [this.agent && this.agent.city || '',[Validators.required]],
    //         hospital_id: [this.agent && this.agent.hospital_id || '',[Validators.required]],
    //         // language: [this.doctor && this.doctor.language || '', [Validators.required]],
    //         gender: [this.agent && this.agent.gender || '',[Validators.required]],
    //         // speciality: [this.doctor && parseInt(this.doctor.speciality) || '', [Validators.required]],
    //         aadhaar_number: [this.agent && this.agent.aadhaar_number || '',[Validators.required]],
    //         // image: [''],
    //         // education: [this.doctor && this.doctor.education || ''],
    //     });
    // }


    /**
     * Create New Doctor
     * return {Void}
     */
    addAgent(button): void {
        // eslint-disable-next-line no-
        ;
        this.isLoading = true;
        button.target.disabled = true;
        const data = this.form.value;
        // console.log(data);
        this._doctorService.addAgent(data).subscribe(
            (data) => {
                if(data.status_code==201){
                    this._globalService.showMessage('New representative added');
                    // this._matSnackBar.open('New representative added', 'OK', this._globalService._matSnackBarConfig);
                    this._router.navigate(['/agent']);
                }else{
                    this._globalService.showError(data.status_message);
                    // this._matSnackBar.open(data.status_message, 'OK', this._globalService._matSnackBarConfig);
                    // this._router.navigate(['/agent']);
                }
                
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);

            },
            (err) => {
                this._showErrorService.showError(err);
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);
            }
        );
    }

    /**
     * Update Doctor
     * return{void}
     */
    saveDoctor(button): void {
        
        this.isLoading = true;
        button.target.disabled = true;

        const data = this.form.value;
        // data.name  = data.first_name;
        // data.first_last_name  = data.last_name;
        this._doctorService.updateAgent(data).subscribe(
            (data) => {
                if(data.status_code==200){
                    this._globalService.showMessage('Representative Updated Successfully');
                    // this._matSnackBar.open('Representative Updated Successfully', 'OK', this._globalService._matSnackBarConfig);
                    this._router.navigate(['/agent']);
                }else{
                    this._globalService.showError(data.status_message);
                    // this._matSnackBar.open(data.status_message, 'OK', this._globalService._matSnackBarConfig);
                }
                
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);
            },
            (err) => {
                this._showErrorService.showError(err);
                setTimeout(() => {
                    this.isLoading = false;
                    button.target.disabled = false;
                }, 500);
            });
    }

    getState(): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._doctorService.getState();
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                // this.specialityCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                
                this.state = data;
                // console.log(this.specialites);
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }

    getClinic(): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 30,
                };
                return this._clinicService.getclinics(params.page, params.limit);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                // this.specialityCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                
                this.clinic = data;
                // console.log(this.specialites);
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }

    selectSpecialitiesOpened(): void {
        this.inputSearchCamps.nativeElement.focus();
    }

    selectSpecialitiesClosed(): void {
        this.inputSearchCamps.nativeElement.value = '';
        this.setCamps();
    }

    onKey({target: {value}}): void {
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
            this.setCamps(value);
        }, 400);
    }

    setCamps(value = ''): void {
        this._clinicService.searchClinic(0, 20, 'search=' + value).subscribe({
            next: (data) => {
                this.isLoading = false;
                console.log('[agent-form.component.ts || Line no. 234 ....]', data);
                if (data.status_code === 200) {
                    this.camps = data['result'].data;
                    // this.totalItemsCount = data['result'].count;
                    // this.loadedData.push(...data['result'].data);
                }
                if (data.status_code === 404) {
                    this.camps = [];
                }
            }
        });
    }

    getCity(state): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._doctorService.getCity(state);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                // this.specialityCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                
                this.city = data;
                // console.log(this.specialites);
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }
}


/*
* Navigation Toggle button Removed
* Navigation Image Logo size Reduced
* Doctor dashboard Good morning text and date Dynamics
* Doctor form fixed
* Toolbar, Doctor dashboard name word capitalized
*  */
