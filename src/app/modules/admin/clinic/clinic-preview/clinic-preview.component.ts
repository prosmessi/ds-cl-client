import {Component, OnInit} from '@angular/core';
import {browserRefresh} from "../../../../app.component";
import {ActivatedRoute} from "@angular/router";
import {DoctorService} from "../../../../services/doctor/doctor.service";

@Component({
    selector: 'app-clinic-preview',
    templateUrl: './clinic-preview.component.html',
    styleUrls: ['./clinic-preview.component.scss']
})
export class ClinicPreviewComponent implements OnInit {

    isLoading: boolean = false;
    doctorId: string;
    doctor: any;

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _doctorService: DoctorService
    ) {
    }

    ngOnInit(): void {
        this.doctorId = this._activatedRoute.snapshot.paramMap.get('doctorId') || '';

        this.doctor = this._doctorService._doctorData;
        if (browserRefresh && JSON.parse(localStorage.getItem('listItem'))) {
            this.doctor = JSON.parse(localStorage.getItem('listItem'));
        }
    }

}
