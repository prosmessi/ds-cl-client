import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Location } from '@angular/common';
import {DoctorService} from '../../../../services/doctor/doctor.service';
import {browserRefresh} from '../../../../app.component';
import {PatientsService} from '../../../../services/patients/patients.service';
import moment from 'moment';

@Component({
  selector: 'app-patients-preview',
  templateUrl: './patients-preview.component.html',
  styleUrls: ['./patients-preview.component.scss']
})
export class PatientsPreviewComponent implements OnInit {

  isLoading: boolean = false;
  doctorId: string;
  doctor: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _patientsService: PatientsService,
    private _location: Location
  ) { }

  ngOnInit(): void {
    this.doctorId = this._activatedRoute.snapshot.paramMap.get('doctorId') || '';

    // this.doctor = this._patientsService._patientData;
    // if (browserRefresh && JSON.parse(localStorage.getItem('listItem'))) {
    //   this.doctor = JSON.parse(localStorage.getItem('listItem'));
    // }
    this.doctor = this._patientsService._patientData;
    // console.log(this.doctor);
    // if(this.doctor.education_details) {
    //   this.doctor.education_details = JSON.parse(this.doctor.education_details);
    // }
  }

  getAge(date): number {
    console.log(date);
    return moment().diff(date, 'years', false)
  }

  goBack() {
    if (window.history.length > 1) {
      this._location.back()
    } else {
      this._router.navigate(['/patients']).then();
    }
  }


}
