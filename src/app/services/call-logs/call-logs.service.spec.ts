import { TestBed } from '@angular/core/testing';

import { CallLogsService } from './call-logs.service';

describe('CallLogsService', () => {
  let service: CallLogsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CallLogsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
