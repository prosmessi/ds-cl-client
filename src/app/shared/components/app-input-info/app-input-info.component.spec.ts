import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppInputInfoComponent } from './app-input-info.component';

describe('AppInputInfoComponent', () => {
  let component: AppInputInfoComponent;
  let fixture: ComponentFixture<AppInputInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppInputInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppInputInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
