import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CallQueueListComponent} from './call-queue-list/call-queue-list.component';
import {CallQueueVideoComponent} from './call-queue-video/call-queue-video.component';
import {Route, RouterModule} from '@angular/router';
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {CdkScrollableModule} from "@angular/cdk/scrolling";
import {MatTableModule} from "@angular/material/table";
import {MatButtonModule} from "@angular/material/button";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatPaginatorModule} from "@angular/material/paginator";
import {CdkAccordionModule} from '@angular/cdk/accordion';
import {SharedModule} from '../../../shared/shared.module';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';

const routes: Route[] = [
    {
        path: '',
        component: CallQueueListComponent
    },
    {
        path: ':handle',
        component: CallQueueVideoComponent,

    },
    {
        path: ':handle/:callId',
        component: CallQueueVideoComponent,

    },
    {
        path: ':preview/:callId/video',
        component: CallQueueVideoComponent,

    },
    {
        path: '**',
        component: CallQueueListComponent,
    }
];

@NgModule({
    declarations: [
        CallQueueListComponent,
        CallQueueVideoComponent
    ],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
		MatProgressBarModule,
		MatFormFieldModule,
		MatIconModule,
		MatInputModule,
		CdkScrollableModule,
		MatTableModule,
		MatButtonModule,
		MatTooltipModule,
		MatNativeDateModule,
		MatPaginatorModule,
		CdkAccordionModule,
		SharedModule,
		MatDatepickerModule
	]
})
export class CallQueueModule {
}
