import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PatientsListComponent} from './patients-list/patients-list.component';
import {PatientsFormComponent} from './patients-form/patients-form.component';
import {PatientsPreviewComponent} from './patients-preview/patients-preview.component';
import {Route, RouterModule} from '@angular/router';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {CdkScrollableModule} from '@angular/cdk/scrolling';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatIconModule} from '@angular/material/icon';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTabsModule} from '@angular/material/tabs';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MedicationListComponent} from './medications/medication-list/medication-list.component';
import {AllergiesListComponent} from './allergies/allergies-list/allergies-list.component';
import {PrescriptionListComponent} from './prescription/prescription-list/prescription-list.component';
import {CallDetailsListComponent} from './call-details/call-details-list/call-details-list.component';
import {PatientSearchListComponent} from './patient-search-list/patient-search-list.component';
import {PatientsService} from "../../../services/patients/patients.service";
import {MedicalDetailsComponent} from "./medical-details/medical-details.component"
import {SharedModule} from '../../../shared/shared.module';

const routes: Route[] = [
    {
        path: '',
        component: PatientsListComponent
    },
    {
        path: ':handle',
        component: PatientsFormComponent,

    },
    {
        path: 'medical/details',
        component: MedicalDetailsComponent,

    },
    {
        path: 'search/list',
        component: PatientSearchListComponent,

    },
    {
        path: ':handle/:patientId',
        component: PatientsFormComponent,

    },
    {
        path: ':preview/:patient/view',
        component: PatientsPreviewComponent,

    },
   
    {
        path: '**',
        component: PatientsListComponent,
    }
];

@NgModule({
    declarations: [
        PatientsListComponent,
        PatientsFormComponent,
        PatientsPreviewComponent,
        MedicationListComponent,
        AllergiesListComponent,
        PrescriptionListComponent,
        CallDetailsListComponent,
        PatientSearchListComponent,
        MedicalDetailsComponent
    ],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
		MatProgressBarModule,
		CdkScrollableModule,
		MatTableModule,
		MatButtonModule,
		MatTooltipModule,
		MatIconModule,
		MatPaginatorModule,
		MatTabsModule,
		ReactiveFormsModule,
		MatFormFieldModule,
		MatInputModule,
		MatSelectModule,
		MatDatepickerModule,
		MatNativeDateModule,
		MatRadioModule,
		SharedModule
	],
    providers: [
        PatientsService
    ]
})
export class PatientsModule {
}
