import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SpecialityListComponent} from './speciality-list/speciality-list.component';
import {Route, RouterModule} from '@angular/router';
import {DoctorsFormComponent} from '../doctors/doctors-form/doctors-form.component';
import { SpecialityFormComponent } from './speciality-form/speciality-form.component';
import {SpecialityService} from "../../../services/speciality/speciality.service";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {CdkScrollableModule} from "@angular/cdk/scrolling";
import {MatTableModule} from "@angular/material/table";
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from '../../../shared/shared.module';

const routes: Route[] = [
    {
        path: '',
        component: SpecialityListComponent
    },
    {
        path: ':handle',
        component: SpecialityFormComponent,

    },
    {
        path: ':handle/:specialityId',
        component: SpecialityFormComponent,

    },
    {
        path: '**',
        component: SpecialityListComponent,
    }
];


@NgModule({
    declarations: [SpecialityListComponent, SpecialityFormComponent],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
		MatPaginatorModule,
		MatProgressBarModule,
		MatFormFieldModule,
		MatIconModule,
		MatInputModule,
		MatButtonModule,
		CdkScrollableModule,
		MatTableModule,
		ReactiveFormsModule,
		SharedModule,
	],
    providers: [
        SpecialityService
    ]
})
export class SpecialityModule {
}
