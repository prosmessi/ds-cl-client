import {FormGroup} from '@angular/forms';
import {ValidationBase} from './validation.class';

interface IDataItem {
	name: string;
}

export interface IValidationMessageObj {
	ifPattern: (msg: string) => string;
	message: string;
}

export abstract class ValidationAbstract extends ValidationBase{

	// getSelected(type: string): void;
	// getSelected(type: string, one: true): IDataItem;
	// getSelected(type: string, one: false): IDataItem[];

	public abstract form: FormGroup;

	protected constructor() {
		super();
	}

}
