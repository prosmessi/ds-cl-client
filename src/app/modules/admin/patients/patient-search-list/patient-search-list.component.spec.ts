import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientSearchListComponent } from './patient-search-list.component';

describe('PatientSearchListComponent', () => {
  let component: PatientSearchListComponent;
  let fixture: ComponentFixture<PatientSearchListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatientSearchListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientSearchListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
