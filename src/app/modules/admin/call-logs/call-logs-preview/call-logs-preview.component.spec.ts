import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CallLogsPreviewComponent } from './call-logs-preview.component';

describe('CallLogsPreviewComponent', () => {
  let component: CallLogsPreviewComponent;
  let fixture: ComponentFixture<CallLogsPreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CallLogsPreviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CallLogsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
