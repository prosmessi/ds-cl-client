export const environment = {
    production: true,
    // baseUrl: 'https://deshclinic.com:3001/',
    baseUrl: 'https://deshclinics.in:3001/',
    baseImageUrl: 'https://deshclinics.in/api/'
};
