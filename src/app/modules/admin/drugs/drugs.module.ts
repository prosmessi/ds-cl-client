import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DrugsListComponent} from './drugs-list/drugs-list.component';
import {Route, RouterModule} from '@angular/router';
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatTableModule} from "@angular/material/table";
import {CdkScrollableModule} from "@angular/cdk/scrolling";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatButtonModule} from "@angular/material/button";
import {MatTooltipModule} from "@angular/material/tooltip";

const routes: Route[] = [
    {
        path: '',
        component: DrugsListComponent
    }
];

@NgModule({
    declarations: [
        DrugsListComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatProgressBarModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatTableModule,
        CdkScrollableModule,
        MatPaginatorModule,
        MatButtonModule,
        MatTooltipModule,
    ]
})
export class DrugsModule {
}
