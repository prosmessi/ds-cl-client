import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CallDetailsListComponent } from './call-details-list.component';

describe('CallDetailsListComponent', () => {
  let component: CallDetailsListComponent;
  let fixture: ComponentFixture<CallDetailsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CallDetailsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CallDetailsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
