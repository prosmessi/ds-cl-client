import {Route} from '@angular/router';
import {AuthGuard} from './core/auth/guards/auth.guard';
import {InitialDataResolver} from './app.resolvers';
import {LayoutComponent} from './layout/layout.component';
import {AvailabilityFormComponent} from './modules/admin/doctors/set-availability/availability-form.component';

// @formatter:off
// tslint:disable:max-line-length
export const appRoutes: Route[] = [

    // Redirect empty path to '/dashboard', then it will verify for loggedIn status
    {path: '', pathMatch: 'full', redirectTo: 'dashboard'},

    // Auth routes
    {
        path: '',
        children: [

            {
                path: 'forgot-password',
                loadChildren: () => import('./modules/auth/forgot-password/forgot-password.module').then(m => m.AuthForgotPasswordModule)
            },
            {
                path: 'sign-in',
                loadChildren: () => import('./modules/auth/sign-in/sign-in.module').then(m => m.AuthSignInModule)
            },
        ]
    },

    // Admin routes
    {
        path: '',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        component: LayoutComponent,
        resolve: {
            initialData: InitialDataResolver,
        },
        children: [
            {
                path: 'dashboard',
                loadChildren: () => import('./modules/admin/dashboard/dashboard.module').then(m => m.DashboardModule)
            },
            {
                path: 'doctors',
                loadChildren: () => import('./modules/admin/doctors/doctors.module').then(m => m.DoctorsModule)
            },
            {
                path: 'set-availability',
                component: AvailabilityFormComponent
            },
            {
                path: 'agent',
                loadChildren: () => import('./modules/admin/agent/agent.module').then(m => m.AgentModule)
            },
            {
                path: 'patients',
                loadChildren: () => import('./modules/admin/patients/patients.module').then(m => m.PatientsModule)
            },
            {
                path: 'appointments',
                loadChildren: () => import('./modules/admin/appointments/appointments.module').then(m => m.AppointmentsModule)
            },
            {
                path: 'call-queue',
                loadChildren: () => import('./modules/admin/call-queue/call-queue.module').then(m => m.CallQueueModule)
            },
            {
                path: 'rating-review',
                loadChildren: () => import('./modules/admin/rating-review/rating-review.module').then(m => m.RatingReviewModule)
            },
             {
                path: 'clinic',
                loadChildren: () => import('app/modules/admin/clinic/clinic.module').then(m => m.ClinicModule)
            },
            // {
            //     path: 'drugs',
            //     loadChildren: () => import('app/modules/admin/drugs/drugs.module').then(m => m.DrugsModule)
            // },
            {
                path: 'call-logs',
                loadChildren: () => import('./modules/admin/call-logs/call-logs.module').then(m => m.CallLogsModule)
            },
            {
                path: 'speciality',
                loadChildren: () => import('./modules/admin/speciality/speciality.module').then(m => m.SpecialityModule)
            },
            {
                path: 'reset-password',
                loadChildren: () => import('./modules/admin/reset-password/reset-password.module').then(m => m.AuthResetPasswordModule)
            },
        ]
    }
];
