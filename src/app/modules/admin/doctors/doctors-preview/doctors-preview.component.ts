import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {browserRefresh} from '../../../../app.component';
import {ActivatedRoute, Router} from '@angular/router';
import {DoctorService} from '../../../../services/doctor/doctor.service';
import {SpecialityService} from '../../../../services/speciality/speciality.service';
import {Pagination} from '../../../../shared/classes/pagination.class';
import {Observable} from 'rxjs';
import {GlobalService} from '../../../../services/global/global.service';
import {Location} from '@angular/common';
import {TableExportToExcelDirective} from '../../../../shared/directives/table-export-to-excel.directive';

@Component({
    selector: 'app-doctors-preview',
    templateUrl: './doctors-preview.component.html',
    styleUrls: ['./doctors-preview.component.scss']
})
export class DoctorsPreviewComponent extends Pagination implements OnInit, AfterViewInit {
    public httpReq: (page: number, limit: number, queryField: string) => Observable<any>;

    isLoading: boolean = false;
    doctorId: string;
    doctor: any;
    specialites: Array<any> = [];
    doctorReviews = [];

    constructor(
      private _activatedRoute: ActivatedRoute,
      private _doctorService: DoctorService,
      private _specialityService: SpecialityService,
      private _location: Location,
      private _router: Router,
      private _globalService: GlobalService
    ) {
        super();
        this.httpReq = (page, limit, queryField): Observable<any> => this._globalService.searchRatings(page, limit, queryField);
    }

    ngOnInit(): void {
        this.setSpecialities();
        this.doctorId = this._activatedRoute.snapshot.paramMap.get('doctorId') || '';

        this.doctor = this._doctorService._doctorData;
        if (browserRefresh && JSON.parse(localStorage.getItem('listItem'))) {
            this.doctor = JSON.parse(localStorage.getItem('listItem'));
        }
        this.doctor.avgrating = Math.round(+this.doctor?.avgrating);
        if(this.doctor.education_details) {
            this.doctor.education_details = JSON.parse(this.doctor.education_details);
        }
        this.queryField = 'doctor_id=' + this.doctorId;
        this.limit = 5;
    }

    ngAfterViewInit(): void {
        this.fetchForFirstTime().then();
    }

    getSpecialityById(id: string | number): {name: string; id: number} {
        return this.specialites.find(c => c.id == id);
    }

    goBack() {
        if (window.history.length > 1) {
            this._location.back()
        } else {
            this._router.navigate(['/doctors']).then();
        }
    }

    setSpecialities(value = ''): void {
        this._specialityService.searchSpeciality(0, 20, 'search=' + value).subscribe({
            next: (data) => {
                this.isLoading = false;

                if (data.status_code === 200) {
                    this.specialites = data['result'].data;
                    // this.totalItemsCount = data['result'].count;
                    // this.loadedData.push(...data['result'].data);
                }
                if (data.status_code === 404) {
                    this.specialites = [];
                }
            }
        });
    }

    // getReviewsForDoctor(): void {
    //     this.isLoading = true;
    //     this._doctorService.getDoctorRating(0, 200, this.doctorId)
    //       .subscribe({
    //           next: (response) => {
    //             this.isLoading = false;
    //             if(response.status_code === 200) {
    //                 this.doctorReviews = response.result.data;
    //                 console.log('[doctors-preview.component.ts || Line no. 69 ....]', response);
    //             }
    //           },
    //           error: (error) => {
    //               this.isLoading = false;
    //               console.log(error);
    //           }
    //       })
    // }
}
