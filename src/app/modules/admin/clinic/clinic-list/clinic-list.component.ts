import { AfterViewInit, Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { map, startWith, switchMap } from 'rxjs/operators';
import { ClinicService } from '../../../../services/clinic/clinic.service';
import { MatTableDataSource } from '@angular/material/table';
import { merge, Observable } from 'rxjs';
// import {DoctorService} from '../../../../services/doctor/doctor.service';
import { fuseAnimations } from '../../../../../@fuse/animations';
import { Pagination } from '../../../../shared/classes/pagination.class';
import { GlobalService } from '../../../../services/global/global.service';
import {ShowErrorService} from '../../../../services/show-error/show-error.service';
import {TableExportToExcelDirective} from '../../../../shared/directives/table-export-to-excel.directive';

@Component({
    selector: 'app-clinic-list',
    templateUrl: './clinic-list.component.html',
    styleUrls: ['./clinic-list.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class ClinicListComponent extends Pagination implements OnInit, AfterViewInit {
    public httpReq: (page: number, limit: number, queryField: string) => Observable<any>;

    doctorsCount: number = 0;
    doctorsTableColumns: string[] = ['id', 'namePhone', 'city', 'state', 'enable_payment', 'action'];
    doctors: Array<any> = [];

    @ViewChild(TableExportToExcelDirective) table: TableExportToExcelDirective;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;


    /**
     * Constructor
     */
    constructor(
        private _router: Router,
        private _globalService: GlobalService,
        // private _doctorService: DoctorService,
        private _showErrorService: ShowErrorService,
        private _clinicService: ClinicService
    ) {
        super();
        this.httpReq = (page, limit, queryField): Observable<any> => this._clinicService.searchClinic(page, limit, queryField);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._globalService.setState();
        this.doctorsCount = 1;
        // console.log('This is the http request function', this._clinicService.searchClinic(1,10, ''));
    }

    /**
     * After View Init
     */
    ngAfterViewInit(): void {
        // this.getDoctorsList();
        this.fetchForFirstTime().then();
    }

    /**
     * Get Doctors List
     * return {Void}
     */

    getDoctorsList(): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => {
                this.isLoading = true;
                const params = {
                    page: 1,
                    limit: 20,
                };
                return this._clinicService.getclinics(params.page, params.limit);
            }),
            map((data) => {
                this.isLoading = false;
                // set pagination total count
                this.doctorsCount = data['result'].length;

                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                this.doctors = data;
                /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
                 this.dataSource.sort = this.sort;*/
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }


    /**
     * Track by function for ngFor loops
     *
     * @param index
     * @param item
     */
    trackByFn(index: number, item: any): any {
        return item.id || index;
    }

    /**
     * Edit Doc info
     *
     * @param doctor
     */
    editClinic(clinic): void {

        localStorage.setItem('listItem', JSON.stringify(clinic));
        this._clinicService._clinicData = clinic;
        this._router.navigate([`/clinic/edit/${clinic.id}`]);
    }

    /**
     * Preview doctor's info
     *
     * @param doctor
     */
    previewClinic(clinic): void {
        localStorage.setItem('listItem', JSON.stringify(clinic));
        this._clinicService._clinicData = clinic;
        this._router.navigate([`/clinic/preview/${clinic.id}/view`]);
    }

    deleteClinic(button, id, index): void {
        this.isLoading = true;
        button.target.disabled = true;


        // const data = this.specialityForm.value;
        // const data = {
        //     id:dataObj.id
        // }
        // return;
        this._clinicService.deleteClinic(id).subscribe(
          (res) => {
              // this._matSnackBar.open('Doctor Deleted Successfully', 'OK', this._globalService._matSnackBarConfig);
              // this._router.navigate(['/speciality']);
              if(res.status_code === 200) {
                  this._globalService.showMessage('Clinic Deleted Successfully');
                  this.deleteItem(index).then(() => {
                      this.isLoading = false;
                      button.target.disabled = false;
                  });
              } else {
                  this._globalService.showError(res.status_message);
              }
              //    delete dataObj;
              // this.doctors =  this.doctors.filter(person => person.users_id !=dataObj );
              //     setTimeout(() => {
              //         this.isLoading = false;
              //         button.target.disabled = false;
              //     }, 500);
          },
          (err) => {
              this._showErrorService.showError(err);
              setTimeout(() => {
                  this.isLoading = false;
                  button.target.disabled = false;
              }, 500);
          });
    }

}
