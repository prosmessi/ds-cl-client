import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable({
    providedIn: 'root'
})
export class ShowErrorService {
    private showAlert = false;

    /**
     * Constructor
     * @param {MatSnackBar} _matSnackBar
     */
    constructor(
        private _matSnackBar: MatSnackBar
    ) {
    }


    /**
     * showError will extract the text message from error
     *
     * @param err: any
     * @return {void}
     */
    showError(err): void {
        const errorMsg = {
            status: (err.error && err.error.status) ? err.error.status : '',
            message: (err.error && err.error.message) ? err.error.message : '',
        };

        if (err && err['error'] && err['error'] && err['error'].message) {
            errorMsg.message = err['error'].message;
        } else if (err && err['errorMessage']) {
            errorMsg.message = err['errorMessage'];
        } else if (!navigator.onLine) {
            errorMsg.message = `There is no internet connection, Please check your connection`;
        } else {
            errorMsg.message = 'There were some error(s). Please check your internet connection or try again later';
        }


        this.presentError(errorMsg);
    }

    /**
     *
     * presentError will present the  extracted text message from error in snackbar
     *
     * @param errorMsg: any
     * @return {void}
     */
    presentError(errorMsg): void {
        const errMsg = (errorMsg && errorMsg.message) ? errorMsg.message : errorMsg;
        this._matSnackBar.open(errMsg, '', {
            duration: 3000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: ['red-snackbar']
        });
    }
}
