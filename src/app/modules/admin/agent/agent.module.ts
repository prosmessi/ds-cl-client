import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AgentListComponent} from './agent-list/agent-list.component';
import {AgentFormComponent} from './agent-form/agent-form.component';
import {AgentPreviewComponent} from './agent-preview/agent-preview.component';
import {Route, RouterModule} from '@angular/router';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {CdkScrollableModule} from '@angular/cdk/scrolling';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {DoctorService} from '../../../services/doctor/doctor.service';
import {SharedModule} from '../../../shared/shared.module';

const routes: Route[] = [
    {
        path: '',
        component: AgentListComponent
    },
    {
        path: ':handle',
        component: AgentFormComponent,

    },
    {
        path: ':handle/:doctorId',
        component: AgentFormComponent,

    },
    {
        path: ':preview/:doctorId/view',
        component: AgentPreviewComponent,

    },
    {
        path: '**',
        component: AgentListComponent,
    }
];

@NgModule({
    declarations: [
        AgentListComponent,
        AgentFormComponent,
        AgentPreviewComponent
    ],
	imports: [
		CommonModule,
		RouterModule.forChild(routes),
		MatProgressBarModule,
		MatSortModule,
		MatTableModule,
		MatIconModule,
		MatButtonModule,
		CdkScrollableModule,
		MatFormFieldModule,
		MatInputModule,
		MatPaginatorModule,
		MatTooltipModule,
		ReactiveFormsModule,
		MatSelectModule,
		SharedModule
	],
    providers: [
        DoctorService
    ]
})
export class AgentModule {
}
