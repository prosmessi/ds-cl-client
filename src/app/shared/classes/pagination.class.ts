import {Observable} from 'rxjs';
import {FormGroup} from '@angular/forms';
import {TableExportToExcelDirective} from '../directives/table-export-to-excel.directive';

export abstract class Pagination {

	public isLoading = true;
	public isSearchedList = false;
	public totalItemsCount = 0;
	public limit = 10;
	public currentPage = 1;
	public currentItems = [];
	public queryField = '';

	public table: TableExportToExcelDirective;

	// public fetchState$: Subject<any> = new Subject();
	public abstract httpReq: (page: number, limit: number, queryField: string) => Observable<any>;
	// eslint-disable-next-line @typescript-eslint/member-ordering
	private loadedData = [];


	protected constructor() { }

	/**
	 * @description Fetch the item on the initial load
	 */
	public async fetchForFirstTime(): Promise<void> {
		// Call this method in your component's ngOnAfterViewInit method
		await this.fetchFromDB(this.currentPage - 1, this.limit);
		this.currentItems = this.loadedData;
	}

	public exportJSON() {
		try {
			this.table.exportToExcel();
		} catch(e) {
			if(!(this.table instanceof TableExportToExcelDirective)) {
				console.warn('Use @ViewChild to get the table reference..');
			}
		}
	}

	/**
	 * @description Call after you have successfully query your database
	 * @param index
	 */
	public async deleteItem(index): Promise<void> {
		const noLoad = this.loadedData.length === this.totalItemsCount;
		this.currentItems = this.currentItems.filter((c, ind) => ind !== index);
		this.loadedData = this.loadedData.filter((c, ind) => ((this.currentPage - 1) * this.limit)  + index !== ind);

		if(noLoad) {
			this.totalItemsCount -= 1;
			return;
		}
		// If the items are not loaded ahead for the filling process after deletion
		const isLoaded = this.currentPage * this.limit < this.loadedData.length;
		console.log('isLoaded: ', isLoaded);
		if(!isLoaded) {
			await this.fetchFromDB(this.currentPage * this.limit - 1, 1);
		} else {
			this.totalItemsCount -= 1;
		}
		console.log('[pagination.class.ts || Line no. 51 ....]', this.totalItemsCount, this.loadedData);
		this.currentItems = [...this.currentItems, this.loadedData[this.currentPage * this.limit - 1]];
		// Then fetch from the database
		// Otherwise just load that item
	}

	public async onPageSizeChange(from, to): Promise<void> {
		// let index = this.currentPage * from;
		// console.log(index, newPageAtIndex, from > to);
		const index = (this.currentPage - 1) * this.limit + 1;
		const newPageAtIndex = Math.ceil(index / to);

		// Bug here on current page when going from 10 to 5 at 45-53 [Fixed]
		// if (from > to) newPageAtIndex = this.currentPage;

		this.limit = to;
		this.currentPage = newPageAtIndex;

		// Handle if items are not loaded and starting from new index
		// The next items are not fetched yet from the db
		const cond =
			this.loadedData.length < this.currentPage * to &&
			this.loadedData.length < this.totalItemsCount;

		// console.log(this.currentPage);

		if (cond) {
			const startInd = this.loadedData.length;
			// const end = this.currentPage * to;
			// Bug spotted here [Fixed]
			const arr = await this.fetchFromDB(startInd, this.limit);
			// console.log(arr);
			// this.loadedData.push(...arr);
		}

		const start = (this.currentPage - 1) * this.limit;
		const end = this.currentPage * this.limit;

		// console.log(start, end, this.currentPage);
		this.pushToArr({ start, end });
	}

	public async onPageChange(pageObj): Promise<void> {
		if (pageObj.pageSize !== this.limit) {
			await this.onPageSizeChange(this.limit, pageObj.pageSize);
			return;
		}
		if (pageObj.pageIndex - pageObj.previousPageIndex > 0) {
			await this.next();
		} else {
			await this.prev();
		}
		this.currentPage = pageObj.pageIndex + 1;
	}

	public async handleSearchClick(form: FormGroup): Promise<void> {
		console.log('[pagination.class.ts || Line no. 80 ....]', form);
		this.isLoading = true;
		this.currentPage = 1;
		this.loadedData = [];
		// this.currentItems = [];
		this.prepareQuery(form);
		await this.fetchFromDB(this.currentPage - 1, this.limit);
		this.isSearchedList = true;
		this.currentItems = this.loadedData;
		if(this.queryField === '') {
			this.isSearchedList = false;
		}
	}

	public isLastPage() {
		// CurPage = 3, Limit = 10, TotalCount = 30
		// 3 * 10 = 30 >= 25
		return this.currentPage * this.limit >= this.totalItemsCount;
	}

	private pushToArr({ start, end }): void {
		// console.log(this.loadedData);
		// return;
		this.currentItems = [];
		this.currentItems = this.loadedData.slice(start, end);
		// console.log(this.currentItems);
	}

	private async next(): Promise<void> {
		const cond =
			this.loadedData.length >= (this.currentPage + 1) * this.limit ||
			this.loadedData.length === this.totalItemsCount;
		// console.log((this.currentPage + 1) * this.pageSize, cond, this.currentPage);
		const start = this.currentPage * this.limit;
		const end = (this.currentPage + 1) * this.limit;
		if (!cond) {
			// Logic for iterating one by one
			// Change it in future to jump
			const arr = await this.fetchFromDB(start, this.limit);
			// console.log(arr);
			// this.loadedData.push(...arr);
		}
		// return;
		// console.log(this.loadedData);
		this.pushToArr({ start, end });
	}

	private prev(): void {
		const start = (this.currentPage - 2) * this.limit;
		const end = (this.currentPage - 1) * this.limit;
		this.pushToArr({ start, end });
	}

	private prepareQuery(form: FormGroup): void {
		if(!form) {
			this.queryField = '';
			return;
		}
		let query = '';
		Object.keys(form.controls).forEach((controlName) => {
			const value = form.get(controlName).value;
			if(value) {
				query += `${controlName}=${value}&`;
			}
		});
		this.queryField = query;
		console.log('[pagination.class.ts || Line no. 132 ....]', query);
	}

	private async fetchFromDB(offset, limit): Promise<void> {
			console.log('Fetching from db ');

		this.isLoading = true;


		const params = { page: offset, limit };
		return new Promise((res, rej) => {

			this.httpReq(params.page, params.limit, this.queryField)
				.subscribe({
					next: (data) => {

						this.isLoading = false;
						console.log('[pagination.class.ts || Line no. 171 ....]', data);

						if (data.status_code === 200) {
							this.totalItemsCount = data['result'].count;
							this.loadedData.push(...data['result'].data);
						}
						if (data.status_code === 404) {
							if(this.loadedData?.length === 0) {
								this.totalItemsCount = 0;
							}
						}
						res();
					},
					error: (err) => {
						this.isLoading = false;
						console.log('[pagination.class.ts || Line no. 160 ....]', err);
						rej();
					}
				});
			// this.fetchState$.next({cb: (data) => {
			// 		this.loadingState$.next(false);
			//
			// 		console.log('[pagination.class.ts || Line no. 171 ....]', data);
			//
			// 		if (data.status_code === 200) {
			// 			this.totalItemsCount = data['result'].count;
			// 			this.loadedData.push(...data['result'].data);
			// 		}
			// 		if (data.status_code === 404) {
			// 			if(this.loadedData.length === 0) {
			// 				this.totalItemsCount = 0;
			// 			}
			// 		}
			// 		res();
			// 	}, params});
		});
	}
}
