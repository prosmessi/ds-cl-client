import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppListHeaderComponent } from './app-list-header.component';

describe('AppListHeaderComponent', () => {
  let component: AppListHeaderComponent;
  let fixture: ComponentFixture<AppListHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppListHeaderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppListHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
