import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-allergies-list',
  templateUrl: './allergies-list.component.html',
  styleUrls: ['./allergies-list.component.scss']
})
export class AllergiesListComponent implements OnInit {

    isLoading: boolean = false;

    allergieTableColumns: string[] = ['allergieName', 'allergieType'];
    patients = [
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        }, {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        }, {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true
        }, {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            city: 'New Delhi',
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        },


    ];

    constructor() { }

  ngOnInit(): void {
  }

}
