import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {AppointmentService} from '../../../../services/appointment/appointment.service';
import {Router} from '@angular/router';
import {merge, Observable} from 'rxjs';
import {map, startWith, switchMap} from 'rxjs/operators';
import {Pagination} from '../../../../shared/classes/pagination.class';
import {GlobalService} from '../../../../services/global/global.service';
import {FormControl, FormGroup} from '@angular/forms';
import {TableExportToExcelDirective} from '../../../../shared/directives/table-export-to-excel.directive';
@Component({
  selector: 'app-call-queue-list',
  templateUrl: './call-queue-list.component.html',
  styleUrls: ['./call-queue-list.component.scss']
})
export class CallQueueListComponent extends Pagination implements OnInit, AfterViewInit {
  public httpReq: (page: number, limit: number, queryField: string) => Observable<any>;

  isLoading: boolean = false;
  callLogsTableColumns: string[] = ['p_name', 'phone', 'd_name', 'date', 'join', 'checkIn'];
  callLogs: any=[];
  callLogsCount: number = 1;
  loggedInUser: any;
  uuid: string;
  filterForm: FormGroup;
  role: string;
  @ViewChild(TableExportToExcelDirective) table: TableExportToExcelDirective;

  constructor(
    private _appointmentService: AppointmentService,
    public _globalService: GlobalService,
    private _router: Router
  ) {
    super();
    this.role = localStorage.getItem('role');
    let doc_query = '';
    if (this.role && this.role === 'doctor') {
      const curUser = JSON.parse(localStorage.getItem('loggedInUser'))
      doc_query = 'doctor_id=' + curUser.users_id;
    }
    this.httpReq = (page, limit, queryField): Observable<any> => this._globalService.searchCallQueue(page, limit, queryField + doc_query);
  }

  ngOnInit(): void {
    // this.callLogsCount = 1;
    this.loggedInUser = JSON.parse(localStorage.getItem('loggedInUser'));
    this.uuid = this.loggedInUser.uuid;
    this.callWaitingList();

    this.filterForm = new FormGroup({
      search: new FormControl(null, []),
      date: new FormControl(null, []),
    });
  }

  ngAfterViewInit(): void {
    this.fetchForFirstTime().then();
    // this.getAppointmentsList();
  }

  resetSearch(e): void {
    e.stopPropagation();
    this.filterForm.reset();
    this.isSearchedList = false;
    this.handleSearchClick(this.filterForm).then();
  }

  /**
   * Preview doctor's info
   *
   * @param doctor
   */
  previewDetails(doctor): void {
    localStorage.setItem('listItem', JSON.stringify(doctor));
    this._appointmentService._data = doctor;
    this._router.navigate([`/call-queue/preview/${doctor.id}/video`]);
  }

  /**
   * joincall
   */
  joincall(doctor): void {
    // debugger
    localStorage.setItem('listItem', JSON.stringify(doctor));
    this._appointmentService._data = doctor;
    this._router.navigate([`/call-queue/preview/${doctor.id}/video`]);
  }
  //callWaitingList
  callWaitingList(): void {
    // debugger
    // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
    merge().pipe(
      startWith({}),
      switchMap(() => {
        this.isLoading = true;
        const params = {
          uuid : this.uuid,
          page: 1,
          limit: 20,
        };
        return this._appointmentService.callWaitingList(params.uuid);
      }),
      map((data) => {
        console.log('[call-queue-list.component.ts || Line no. 69 ....]', data);
        this.isLoading = false;
        // set pagination total count
        this.callLogsCount = data['result'].length;
        // return response data
        if (data.status_code === 200) {
          return data['result'];
        }

      })
    ).subscribe(
      (data) => {
        // set response data
        // debugger
        this.callLogs = data;
        console.log(this.callLogs);
        /* this.dataSource = new MatTableDataSource(data.map(el => new AuthorModal(el)));
				this.dataSource.sort = this.sort;*/
      },
      (err) => {
        // show the error
        // console.log('err: ', err);
      }
    );
  }
}
