import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { AppUrlsService } from "../../core/app-urls/app-urls.service";
import { Observable } from "rxjs";
import { catchError } from "rxjs/operators";
import { handleError } from "../error-handler/error-handler.service";

@Injectable({
    providedIn: 'root'
})
export class PatientsService {

    _patientData: any;

    constructor(
        private _httpClient: HttpClient,
        private _appUrlService: AppUrlsService
    ) {
    }

    /**
     * Get Patients
     *
     * @returns
     */
    getPatients(page?, limit?, hospitalId?): Observable<any> {
        return this._httpClient.get(this._appUrlService.patients(page, limit, hospitalId)).pipe(
            catchError(handleError)
        );
    }

    /**
     * Get Patients
     *
     * @returns
     */
    getPatientsSearch(page?, limit?, name?): Observable<any> {
        return this._httpClient.get(this._appUrlService.patientsSearch(page, limit, name)).pipe(
            catchError(handleError)
        );
    }

    /**
     * getPatientsPastappointment​
     */
    getPatientsPastappointment​(patient_id?, page?, limit?): Observable<any> {
        return this._httpClient.get(this._appUrlService.getPatientsPastappointment​(patient_id, page, limit)).pipe(
            catchError(handleError)
        );
    }
    /**
   * ADD Doctor
   * @returns {observable<any>}
   */
    addPatient(data): Observable<any> {
        return this._httpClient.post(this._appUrlService.addPatient(), data).pipe(
            catchError(handleError)
        );
    }

    /**
   * ADD Doctor
   * @returns {observable<any>}
   */
    makeCall(data): Observable<any> {
        return this._httpClient.post(this._appUrlService.makeCall(), data).pipe(
            catchError(handleError)
        );
    }

       /**
     * ADD Doctor
     * @returns {observable<any>}
     */
    scheduleAppointment(data): Observable<any> {
        return this._httpClient.post(this._appUrlService.scheduleAppoitment(), data).pipe(
            catchError(handleError)
        );
    }


    /**
        * Delete Doctor
        *
        * @returns
        */
    deletePatients(id): Observable<any> {
        return this._httpClient.delete(this._appUrlService.deleteuser(id)).pipe(
            catchError(handleError)
        );
    }
}
