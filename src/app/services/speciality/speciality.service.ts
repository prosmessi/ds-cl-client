import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AppUrlsService} from "../../core/app-urls/app-urls.service";
import {Observable} from "rxjs";
import {catchError} from "rxjs/operators";
import {handleError} from "../error-handler/error-handler.service";

@Injectable({
    providedIn: 'root'
})
export class SpecialityService {

    _specialityData: any;

    constructor(
        private _httpClient: HttpClient,
        private _appUrlService: AppUrlsService
    ) {
    }

    /**
     * Get Doctor
     *
     * @returns
     */
    getSpeciality(page?, limit?): Observable<any> {
        return this._httpClient.get(this._appUrlService.speciality(page, limit)).pipe(
            catchError(handleError)
        );
    }

    /**
     * Search Speciality
     *
     * @returns
     */
    searchSpeciality(page?, limit?, queryField?): Observable<any> {
        return this._httpClient.get(this._appUrlService.searchSpeciality(page, limit, queryField)).pipe(
          catchError(handleError)
        );
    }

    addSpeciality(data): Observable<any> {
        return this._httpClient.post(this._appUrlService.addSpeciality(), data).pipe(
            catchError(handleError)
        );
    }

    updateSpecialityStatus(data): Observable<any> {
        const doctorId = data.id;
        // delete data['_id'];
        return this._httpClient.put(this._appUrlService.updateSpecialityStatus(doctorId), data).pipe(
            catchError(handleError)
        );
    }

    deleteSpeciality(data): Observable<any> {
        // const doctorId = data.id;
        // delete data['_id'];
        return this._httpClient.delete(this._appUrlService.deleteSpeciality(data)).pipe(
            catchError(handleError)
        );
    }
}
