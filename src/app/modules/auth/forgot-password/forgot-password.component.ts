import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormGroup,
    NgForm,
    ValidationErrors,
    ValidatorFn,
    Validators
} from '@angular/forms';
import {fuseAnimations} from '@fuse/animations';
import {FuseAlertType} from '@fuse/components/alert';
import {AuthService} from 'app/core/auth/auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'auth-forgot-password',
    templateUrl: './forgot-password.component.html',
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class AuthForgotPasswordComponent implements OnInit {
    @ViewChild('forgotPasswordNgForm') forgotPasswordNgForm: NgForm;
    @ViewChild('verifyOtpNgForm') verifyOtpNgForm: NgForm;
    @ViewChild('changePasswordNgForm') changePasswordNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type: 'success',
        message: ''
    };
    forgotPasswordForm: FormGroup;
    verifyOtpForm: FormGroup;
    changePasswordForm: FormGroup;
    showAlert: boolean = false;
    sendOtpLoading = false;
    verifyOtpLoading = false;
    changePasswordLoading = false;
    curUserId = null;
    // superAdminRole = 1;
    step = 1;

    /**
     * Constructor
     */
    constructor(
      private _activatedRoute: ActivatedRoute,
      private _authService: AuthService,
      private _formBuilder: FormBuilder,
      private _router: Router
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.changePasswordForm = this._formBuilder.group({
            password: ['', [Validators.required]],
            conPassword: ['', [Validators.required, this.confirmPassword()]],
        });
        // Create the form
        this.forgotPasswordForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
        });
        this.verifyOtpForm = this._formBuilder.group({
            otp: ['', [Validators.required, this.otpLength()]],
        });
    }

    confirmPassword(): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            // console.log(control.parent?.value?.password);
            const pass = control.parent?.value?.password;
            const confirmPass = control.value;
            return pass !== confirmPass ? {notMatch: true} : null;
            // return {notMatch: true};
        };
    }

    otpLength(): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            // console.log(control.parent?.value?.password);
            // console.log(control);
            if(control.value?.toString().length === 6) {
                return null;
            }
            return {invalidLength: true};
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    verifyOtp(): void {
        console.log('[sign-in.component.ts || Line no. 60 ....]', this.verifyOtpForm);
        // Return if the form is invalid
        if (this.verifyOtpForm.invalid) {
            return;
        }
        this.verifyOtpLoading = true;

        // Disable the form
        this.verifyOtpForm.disable();

        // Hide the alert
        this.showAlert = false;

        // Sign in
        this._authService.verifyOtp({otp: this.verifyOtpForm.value.otp, email: this.forgotPasswordForm.value.email})
          .subscribe(
            (res) => {
                this.verifyOtpLoading = false;
                console.log('[forgot-password.component.ts || Line no. 71 ....]', res);
                if (res.status_code === 200) {
                    this.step = 3;
                    this.curUserId = res.result.userId;
                    this.alert = {
                        type: 'success',
                        message: res.status_message
                    };
                    this.showAlert = true;
                    // const redirectURL = this._activatedRoute.snapshot.queryParamMap.get('redirectURL') || '/dashboard';
                    // this._router.navigateByUrl(redirectURL);
                } else {
                    this.verifyOtpForm.enable();
                    this.verifyOtpNgForm.resetForm();
                    this.alert = {
                        type: 'error',
                        message: res.status_message
                    };
                    this.showAlert = true;
                    // Setting the default value for the type as there is no option to reselect the role
                    // this.forgotPasswordForm.controls['type'].patchValue('admin');
                }

            },
            (response) => {
                this.verifyOtpLoading = false;
                console.log('[forgot-password.component.ts || Line no. 89 ....]', response);
                // Re-enable the form
                this.forgotPasswordForm.enable();

                // Reset the form
                this.forgotPasswordNgForm.resetForm();
                // Setting the default value for the type as there is no option to reselect the role
                // this.forgotPasswordForm.controls['type'].patchValue('admin');

                // Set the alert
                this.alert = {
                    type: 'error',
                    message: 'Something went wrong, Please try again later'
                };

                // Show the alert
                this.showAlert = true;
            }
          );

    }

    changePassword(): void {
        console.log('[sign-in.component.ts || Line no. 60 ....]', this.changePasswordForm);
        // Return if the form is invalid
        if (this.changePasswordForm.invalid) {
            return;
        }
        this.changePasswordLoading = true;

        // Disable the form
        this.changePasswordForm.disable();

        // Hide the alert
        this.showAlert = false;

        // Sign in
        this._authService.changePassword({password: this.changePasswordForm.value.password, user_id: this.curUserId})
          .subscribe(
            (res) => {
                this.changePasswordLoading = false;
                console.log('[forgot-password.component.ts || Line no. 71 ....]', res);
                if (res.status_code === 200) {
                    // const redirectURL = this._activatedRoute.snapshot.queryParamMap.get('redirectURL') || '/dashboard';
                    this.alert = {
                        type: 'success',
                        message: res.status_message
                    };
                    this.showAlert = true;

                    setTimeout(() => {
                        this._router.navigateByUrl('/sign-in');
                    }, 500);
                } else {
                    this.changePasswordForm.enable();
                    this.changePasswordNgForm.resetForm();
                    this.alert = {
                        type: 'error',
                        message: res.status_message
                    };
                    this.showAlert = true;
                    // Setting the default value for the type as there is no option to reselect the role
                    // this.forgotPasswordForm.controls['type'].patchValue('admin');
                }

            },
            (response) => {
                this.changePasswordLoading = false;
                console.log('[forgot-password.component.ts || Line no. 89 ....]', response);
                // Re-enable the form
                this.changePasswordForm.enable();

                // Reset the form
                this.changePasswordNgForm.resetForm();
                // Setting the default value for the type as there is no option to reselect the role
                // this.forgotPasswordForm.controls['type'].patchValue('admin');

                // Set the alert
                this.alert = {
                    type: 'error',
                    message: 'Something went wrong, Please try again later'
                };

                // Show the alert
                this.showAlert = true;
            }
          );
    }

    backToEmail(e): void {
        e.preventDefault();
        this.showAlert = false;
        this.step = 1;
        this.sendOtpLoading = false;
        this.verifyOtpLoading = false;
        this.changePasswordLoading = false;
        // this.changePasswordNgForm.resetForm();
        // this.verifyOtpNgForm.resetForm();
        // this.forgotPasswordNgForm.resetForm();
        this.forgotPasswordForm.controls['email'].patchValue('');
        this.verifyOtpForm.controls['otp'].patchValue('');
    }

    sendEmail(): void {
        console.log('[sign-in.component.ts || Line no. 60 ....]', this.forgotPasswordForm);
        // Return if the form is invalid
        if (this.forgotPasswordForm.invalid) {
            return;
        }
        this.sendOtpLoading = true;

        // Disable the form
        this.forgotPasswordForm.disable();

        // Hide the alert
        this.showAlert = false;

        // Sign in
        this._authService.forgotPassword({...this.forgotPasswordForm.value})
          .subscribe(
            (res) => {
                this.sendOtpLoading = false;
                console.log('[forgot-password.component.ts || Line no. 71 ....]', res);
                if (res.status_code === 200) {
                    this.step = 2;
                    // const redirectURL = this._activatedRoute.snapshot.queryParamMap.get('redirectURL') || '/dashboard';
                    // this._router.navigateByUrl(redirectURL);
                    this.alert = {
                        type: 'success',
                        message: res.status_message
                    };
                    this.forgotPasswordForm.enable();
                    this.showAlert = true;
                } else {
                    this.forgotPasswordForm.enable();
                    this.forgotPasswordNgForm.resetForm();
                    this.alert = {
                        type: 'error',
                        message: res.status_message
                    };
                    this.showAlert = true;
                    // Setting the default value for the type as there is no option to reselect the role
                    // this.forgotPasswordForm.controls['type'].patchValue('admin');
                }

            },
            (response) => {
                this.sendOtpLoading = false;
                console.log('[forgot-password.component.ts || Line no. 89 ....]', response);
                // Re-enable the form
                this.forgotPasswordForm.enable();

                // Reset the form
                this.forgotPasswordNgForm.resetForm();
                // Setting the default value for the type as there is no option to reselect the role
                // this.forgotPasswordForm.controls['type'].patchValue('admin');

                // Set the alert
                this.alert = {
                    type: 'error',
                    message: 'Something went wrong, Please try again later'
                };

                // Show the alert
                this.showAlert = true;
            }
          );
    }
}
