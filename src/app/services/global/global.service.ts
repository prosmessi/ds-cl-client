import { Injectable } from '@angular/core';
import { map, startWith, switchMap } from 'rxjs/operators';
import { merge, Observable } from 'rxjs';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { catchError } from 'rxjs/operators';
import { handleError } from '../error-handler/error-handler.service';
import { HttpClient } from '@angular/common/http';
import { AppUrlsService } from '../../core/app-urls/app-urls.service';
import { FormGroup } from '@angular/forms';
import {environment} from '../../../environments/environment';
import {languages} from '../../mock-api/common/languages/data';

@Injectable({
    providedIn: 'root'
})
export class GlobalService {

    _matSnackBarConfig: MatSnackBarConfig;
    _allStates = [];
    _allCities = [];
    _baseImageUrl = environment.baseImageUrl;

    constructor(
        private _matSnackBar: MatSnackBar,
        private _httpClient: HttpClient,
        private _appUrlService: AppUrlsService
    ) {
        this.setMatSnackBarConfig();
    }

    /**
     * Set Global Mat SnackBar Position and layout
     *
     * @return {void}
     */
    setMatSnackBarConfig(): void {

        this._matSnackBarConfig = {
            duration: 3000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: 'green-snackbar'
        };
    }

    /**
     * Show message globally
     *
     * @return {void}
     */
    showMessage(message: string, panelClass = 'green-snackbar'): void {

        this._matSnackBarConfig.panelClass = panelClass;
        this._matSnackBar.open(message, '', this._matSnackBarConfig);
    }

    /**
     * Show message globally
     *
     * @return {void}
     */
    showError(message: string, panelClass = 'red-snackbar'): void {

        this._matSnackBarConfig.panelClass = panelClass;
        this._matSnackBar.open(message, '', this._matSnackBarConfig);
    }

    /**
     * Setting State
     *
     */

    setState(): void {
        merge().pipe(
            startWith({}),
            switchMap(() => this._httpClient.get(this._appUrlService.state()).pipe(
                catchError(handleError)
            )),
            map((data) => {
                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }


            })
        ).subscribe(
            (data) => {
                // set response data
                this._allStates = data;
            },
            (err) => { }
        );
    }

    setCities(state): void {
        // merge multiple observables, any changes in these observable will call get list method with diff params (sort, order, pageNo)
        merge().pipe(
            startWith({}),
            switchMap(() => this._httpClient.get(this._appUrlService.city(state)).pipe(
                catchError(handleError)
            )),
            map((data) => {
                // return response data
                if (data.status_code === 200) {
                    return data['result'];
                }
            })
        ).subscribe(
            (data) => {
                this._allCities = data;
            },
            (err) => {
                // show the error
                // console.log('err: ', err);
            }
        );
    }

    toSentenceCase(res: string): string { return res.charAt(0).toUpperCase() + res.substring(1).toLowerCase(); }
    /**
     * Get User by role
     *
     * @returns
     */
    getUserSearch(page?, limit?, queryField?): Observable<any> {
        return this._httpClient.get(this._appUrlService.userSearch(page, limit, queryField));
    }
    getLanguages() {
        return languages;
    }
    /**
     * Get User by role
     *
     * @returns
     */
    searchCallLogs(page?, limit?, queryField?): Observable<any> {
        return this._httpClient.get(this._appUrlService.searchCallLogs(page, limit, queryField));
    }
    /**
     * Upload single file
     *
     * @returns
     */
    uploadSingleFile(data): Observable<any> {
        return this._httpClient.post(this._appUrlService.uploadSingleFile(), data, {
            headers: {
                'Content-Type': 'file'
            }
        });
    }
    /**
     * Get Call Queue
     *
     * @returns
     */
    searchCallQueue(page?, limit?, queryField?): Observable<any> {
        return this._httpClient.get(this._appUrlService.searchCallQueue(page, limit, queryField));
    }
    /**
     * Get Ratings
     *
     * @returns
     */
    searchRatings(page?, limit?, queryField?): Observable<any> {
        return this._httpClient.get(this._appUrlService.searchRatings(page, limit, queryField));
    }
    /**
     * Get Single User
     *
     * @returns
     */
    fetchUserById(id): Observable<any> {
        return this._httpClient.get(this._appUrlService.searchUserById(id));
    }
    /**
     * Get Single User
     *
     * @returns
     */
    fetchUpcomingAppointments(page, limit, queryField): Observable<any> {
        return this._httpClient.get(this._appUrlService.fetchUpcomingAppointments(page, limit, queryField));
    }
}
