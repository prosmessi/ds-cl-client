import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-list-by-date',
    templateUrl: './list-by-date.component.html',
    styleUrls: ['./list-by-date.component.scss']
})
export class ListByDateComponent implements OnInit {

    listCount: number = 0;
    listTableColumns: string[] = ['date', 'patients', 'action'];
    lists = [
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true
        },
        {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        }, {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        }, {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Inactive',
            createdAt: 'May 24 2021',
            action: true
        }, {
            id: '7eb7c859-1347-4317-96b6-9476a7e2ba3c',
            name: 'Ajeet Mishra',
            email: 'test@mail.com',
            phone: 9999999999,
            speciality: 'Test',
            experience: 10,
            status: 'Active',
            createdAt: 'May 24 2021',
            action: true
        },


    ];

    constructor() {
    }

    ngOnInit(): void {
        this.listCount = 1;
    }

}
