import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AppUrlsService {

    constructor() {
    }


    public get appUrl(): string {
        return environment.baseUrl;
    }

    public get appUrlAdmin(): string {
        return environment.baseUrl + 'admin/';
    }

    public login(): string {
        return this.appUrlAdmin + 'login';
    }
    public changeByOldPassword(): string {
        return this.appUrl + 'api/change-password';
    }

    public doctor(page, limit, hospitalId): string {
        return this.appUrlAdmin + `getDoctorList/${hospitalId}/${page}/${limit}`;
    }
    public forgetPassword(email): string {
        return this.appUrlAdmin + 'forgot/password/' + email;
    }
    public doctorSearch(page, limit, spec, name): string {
        return this.appUrl + `doctor/Search/${page}/${limit}/${spec}/${name}`;
    }
    public drugsSearch(page, limit, name): string {
        return this.appUrl + `drugs/Search/${page}/${limit}/${name}`;
    }
    public changePassword(): string {
        return this.appUrlAdmin + 'resetPassword';
    }
    public verifyOtp(email, otp): string {
        return this.appUrl + 'user/verifyOtp/' + email + '/' + otp;
    }
    public agent(page, limit): string {
        return this.appUrlAdmin + `getAgentList/${page}/${limit}`;
    }

    public clinic(page, limit): string {
        return this.appUrl + `hospital/allList/${page}/${limit}`;
    }
    public searchClinic(page, limit, queryField): string {
        return this.appUrl + `api/search-clinic/${page}/${limit}?${queryField}`;
    }
    public searchAppointments(page, limit, queryField): string {
        return this.appUrl + `api/search-appointment/${page}/${limit}?${queryField}`;
    }
    public searchCallLogs(page, limit, queryField): string {
        return this.appUrl + `api/search-calllog/${page}/${limit}?${queryField}`;
    }
    public uploadSingleFile(): string {
        return this.appUrl + `upload/singlefile`;
    }
    public searchCallQueue(page, limit, queryField): string {
        return this.appUrl + `api/search-call-queue/${page}/${limit}?${queryField}`;
    }
    public searchRatings(page, limit, queryField): string {
        return this.appUrl + `api/search-ratings/${page}/${limit}?${queryField}`;
    }
    public searchUserById(id): string {
        return this.appUrl + `api/user/${id}`;
    }
    public fetchUpcomingAppointments(page, limit, queryField): string {
        return this.appUrl + `api/appointments/upcoming/${page}/${limit}?${queryField}`;
    }
    public searchSpeciality(page, limit, queryField): string {
        return this.appUrl + `api/search-speciality/${page}/${limit}?${queryField}`;
    }
    public userSearch(page, limit, queryField): string {
        return this.appUrl + `api/search-user/${page}/${limit}?${queryField}`;
    }
    public activeSpeciality(page, limit): string {
        return environment.baseUrl + `patient/specialities/list`;
    }

    public city(state): string {
        return environment.baseUrl + `admin/city/${state}`;
    }

    public state(): string {
        return environment.baseUrl + `admin/state`;
    }

    public getAllocatedTimeSlot(doctor_id, date): string {
        return environment.baseUrl + `doctor/getAllocatedTimeSlot/${doctor_id}/${date}`;
    }
    // public clinic(): string {
    //     return environment.baseUrl + `admin/state`;
    // }

    public addDoctor(): string {
        return this.appUrl + 'doctor/signup';
    }
    public createPdf(): string {
        return this.appUrl + 'hospital/createPdf';
    }
    public updateAvailability(uuid): string {
        return this.appUrl + 'doctor/updateAvailability/' + uuid;
    }
    public addPatient(): string {
        return this.appUrlAdmin + 'addPatient';
    }
    public makeCall(): string {
        return this.appUrl + 'patient/makeCall';
    }
    public scheduleAppoitment(): string {
        return this.appUrl + 'appointment/schedule';
    }
    public addAgent(): string {
        return this.appUrl + 'representative/signUp';
    }

    public addClinic(): string {
        return this.appUrl + 'hospital/createHospital';
    }
    public agentId(agentId): string {
        return this.appUrl + `agent/update/${agentId}`;
    }
    public doctorId(doctorId): string {
        return this.appUrl + `doctor/update/${doctorId}`;
    }
    public hospitalId(hospitalId): string {
        return this.appUrl + `hospital/update/${hospitalId}`;
    }

    public patients(page, limit, hospitalId): string {
        return this.appUrlAdmin + `getPatientList/${hospitalId}/${page}/${limit}`;
    }
    public patientsSearch(page, limit, name): string {
        return this.appUrl + `patient/Search/${page}/${limit}/${name}`;
    }

    public getPatientsPastappointment​(patient_id, page, limit): string {
        return environment.baseUrl + `patient/past/appointment/${patient_id}/${page}/${limit}`;
    }

    public appointments(page, limit, hospitalId): string {
        return this.appUrlAdmin + `appointment/upcoming/${page}/${limit}/${hospitalId}`;
    }

    public pastAppointments(page, limit, hospitalId): string {
        return this.appUrlAdmin + `appointment/past/${page}/${limit}/${hospitalId}`;
    }

    public speciality(page, limit): string {
        return this.appUrlAdmin + `speciality`;
    }

    public addSpeciality(): string {
        return this.appUrl + 'specialities/create';
    }

    public updateSpecialityStatus(specialityId): string {
        return this.appUrl + `specialities/activeInactive/${specialityId}`;
    }

    public deleteSpeciality(specialityId): string {
        return this.appUrl + `specialities/delete/${specialityId}`;
    }
    public deleteuser(id): string {
        return this.appUrl + `user/delete/${id}`;
    }

    public deleteAppointment(id): string {
        return this.appUrl + `appointment/delete/${id}`;
    }

    public appointmentMakeCall(): string {
        return this.appUrl + `patient/appointment/makeCall`;
    }
    //
    public callWaitingList​(uuid): string {
        return environment.baseUrl + `doctor/callWaitingList/${uuid}`;
        // return this.appUrlAdmin + `doctor/callWaitingList​/${uuid}`;
    }

    //getRtcAccessToken
    public getRtcAccessToken(): string {
        return environment.baseUrl + `common/getRtcAccessToken`;
    }

    //updateCallStatus
    public updateCallStatus(): string {
        return environment.baseUrl + `doctor/updated/callStatus`;
    }
    //Get Dashboard count
    public getDashboardCount(type): string {
        return environment.baseUrl + `admin/dashboard/count/${type}`;
    }
    public deleteCamp(campId): string {
        return this.appUrl + `hospital/delete/${campId}`;
    }

}
