import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentPreviewComponent } from './agent-preview.component';

describe('DoctorsPreviewComponent', () => {
  let component: AgentPreviewComponent;
  let fixture: ComponentFixture<AgentPreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgentPreviewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgentPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
